/*
 * Copyright (c) 2020 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef IXBLUE_STDBIN_PARSER_HPP
#define IXBLUE_STDBIN_PARSER_HPP

#include <cola2_lib/io/tcp_socket.h>

#include <algorithm>
#include <cstdint>
#include <iostream>
#include <memory>
#include <numeric>
#include <stdexcept>
#include <vector>

#include "ixblue_stdbin_raw_data.hpp"
#include "pod_struct_serialization.hpp"

inline IxblueStdBinRawData parseNavigationTelegram(std::shared_ptr<cola2::io::TcpSocket> socket,
                                                   const unsigned int timeout)
{
  // Read the header
  NavigationHeader nav_header;
  cola2::io::DataBuffer header_buffer;
  try
  {
    socket->read(header_buffer, sizeof(nav_header), timeout);
  }
  catch (const std::exception &ex)
  {
    socket->reconnect();
    throw std::runtime_error(std::string("Could not read from socket: ") + ex.what());
  }

  // Deserialize header
  serializeOrDeserialize(reinterpret_cast<const unsigned char *>(header_buffer.data()),
                         reinterpret_cast<unsigned char *>(&nav_header), sizeof(nav_header), nav_header.field_sizes());

  // Telegram sizes includes the size of the header and the checkshum
  std::size_t body_size = nav_header.telegram_size - sizeof(nav_header) - sizeof(std::uint32_t);

  // Read the body and checksum
  cola2::io::DataBuffer body_buffer, checksum_buffer;
  body_buffer.reserve(body_size);
  try
  {
    // Read body_size and checksum
    socket->read(body_buffer, body_size, timeout);
    socket->read(checksum_buffer, sizeof(std::uint32_t), timeout);
  }
  catch (const std::exception &ex)
  {
    socket->reconnect();
    throw std::runtime_error(std::string("Could not read body and checksum from socket: ") + ex.what());
  }

  // Deserialize received checksum
  std::uint32_t received_checksum;
  const std::vector<std::size_t> checksum_field_sizes = { sizeof(received_checksum) };
  serializeOrDeserialize(reinterpret_cast<const unsigned char *>(checksum_buffer.data()),
                         reinterpret_cast<unsigned char *>(&received_checksum), sizeof(received_checksum),
                         checksum_field_sizes);

  // Compute checksum from data
  const std::uint32_t sum_h = std::accumulate(header_buffer.begin(), header_buffer.end(), 0);
  const std::uint32_t sum_b = std::accumulate(body_buffer.begin(), body_buffer.end(), 0);
  const std::uint32_t checksum_from_data = sum_h + sum_b;

  // Parse the telegram and fill raw data structure
  IxblueStdBinRawData raw_data;
  if (received_checksum == checksum_from_data)
  {
    // Create index to keep track of where we are reading in the body buffer
    std::size_t index_body_buffer = 0;

    // Parse normal navigation data
    for (std::size_t i = 0; i < IxblueStdBinRawData::basic_navigation_elements; ++i)
    {
      if (nav_header.nav_block_mask & (1 << i))
      {
        serializeOrDeserialize(&body_buffer[index_body_buffer], raw_data.bn_pointers[i], raw_data.bn_sizes[i],
                               raw_data.bn_field_sizes[i]);
        index_body_buffer += raw_data.bn_sizes[i];
      }
    }

    // Parse extended navigation data
    for (std::size_t i = 0; i < IxblueStdBinRawData::extended_navigation_elements; ++i)
    {
      if (nav_header.extended_nav_block_mask & (1 << i))
      {
        serializeOrDeserialize(&body_buffer[index_body_buffer], raw_data.en_pointers[i], raw_data.en_sizes[i],
                               raw_data.en_field_sizes[i]);
        index_body_buffer += raw_data.en_sizes[i];
      }
    }
  }
  else
  {
    throw std::runtime_error("Invalid checksum received");
  }

  // WARN: Take care of what the copy constructor does here... Only the data is valid after this,
  // as the pointers point to the stack memory that is reused after the function returns
  return raw_data;
}

#endif  // IXBLUE_STDBIN_PARSER_HPP
