/*
 * Copyright (c) 2020 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef IXBLUE_STDBIN_NAVIGATION_TELEGRAM_HPP
#define IXBLUE_STDBIN_NAVIGATION_TELEGRAM_HPP

#include <bitset>
#include <climits>
#include <cstdint>
#include <iostream>
#include <map>
#include <numeric>

/*
 * Output navigation header
 */
struct NavigationHeader
{
  std::uint8_t header_id1;
  std::uint8_t header_id2;
  std::uint8_t version;
  std::uint32_t nav_block_mask;
  std::uint32_t extended_nav_block_mask;
  std::uint32_t external_data_mask;
  std::uint16_t telegram_size;
  std::uint32_t validity_time;
  std::uint32_t counter;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = {
      sizeof(std::uint8_t),  sizeof(std::uint8_t),  sizeof(std::uint8_t),  sizeof(std::uint32_t), sizeof(std::uint32_t),
      sizeof(std::uint32_t), sizeof(std::uint16_t), sizeof(std::uint32_t), sizeof(std::uint32_t),
    };
    return sizes;
  }

  NavigationHeader()
  {
    const std::vector<std::size_t> sizes = field_sizes();
    const std::size_t sum = std::accumulate(sizes.begin(), sizes.end(), 0);
    if (sum != sizeof(NavigationHeader))
      std::cerr << "Size mismatch in NavigationHeader" << std::endl;
  }

  friend std::ostream& operator<<(std::ostream&, const NavigationHeader&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const NavigationHeader& d)
{
  os << "-- Input Navigation Header ------" << std::endl
     << "Header Id1: " << d.header_id1 << std::endl
     << "Header Id2: " << d.header_id2 << std::endl
     << "Version: " << static_cast<int>(d.version) << std::endl
     << "Navigation block mask: " << std::bitset<32>(d.nav_block_mask) << std::endl
     << "Extended navigation block mask: " << std::bitset<32>(d.extended_nav_block_mask) << std::endl
     << "External data mask: " << std::bitset<32>(d.external_data_mask) << std::endl
     << "Telegram size: " << d.telegram_size << std::endl
     << "Validity time: " << d.validity_time << std::endl
     << "Counter: " << d.counter << std::endl
     << "---------------------------------";
  return os;
}

/*
 * Input Navigation header
 */
struct InputNavigationHeader
{
  std::uint8_t header_id1 = 'I';
  std::uint8_t header_id2 = 'X';
  std::uint8_t version = 0x03;
  std::uint32_t nav_block_mask = 0x00000000;
  std::uint32_t extended_nav_block_mask = 0x00000000;
  std::uint32_t external_data_mask;
  std::uint16_t telegram_size;
  std::uint8_t time_stamp_reference = 0x00;
  std::uint8_t rfu[7] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = {
      sizeof(std::uint8_t),  sizeof(std::uint8_t),  sizeof(std::uint8_t), sizeof(std::uint32_t),    sizeof(std::uint32_t),
      sizeof(std::uint32_t), sizeof(std::uint16_t), sizeof(std::uint8_t), 7 * sizeof(std::uint8_t),
    };
    return sizes;
  }

  InputNavigationHeader()
  {
    const std::vector<std::size_t> sizes = field_sizes();
    const std::size_t sum = std::accumulate(sizes.begin(), sizes.end(), 0);
    if (sum != sizeof(InputNavigationHeader))
      std::cerr << "Size mismatch in InputNavigationHeader" << std::endl;
  }

  friend std::ostream& operator<<(std::ostream&, const InputNavigationHeader&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const InputNavigationHeader& d)
{
  os << "-- Input Navigation Header ------" << std::endl
     << "Header Id1: " << d.header_id1 << std::endl
     << "Header Id2: " << d.header_id2 << std::endl
     << "Version: " << static_cast<int>(d.version) << std::endl
     << "Navigation block mask: " << std::bitset<32>(d.nav_block_mask) << std::endl
     << "Extended navigation block mask: " << std::bitset<32>(d.extended_nav_block_mask) << std::endl
     << "External data mask: " << std::bitset<32>(d.external_data_mask) << std::endl
     << "Telegram size: " << d.telegram_size << std::endl
     << "Time stamp reference: " << static_cast<int>(d.time_stamp_reference) << std::endl
     << "RFU: " << static_cast<int>(d.rfu[0]) << ", "
                << static_cast<int>(d.rfu[1]) << ", "
                << static_cast<int>(d.rfu[2]) << ", "
                << static_cast<int>(d.rfu[3]) << ", "
                << static_cast<int>(d.rfu[4]) << ", "
                << static_cast<int>(d.rfu[5]) << ", "
                << static_cast<int>(d.rfu[6]) << std::endl
     << "---------------------------------";
  return os;
}

/**********************************************************************************************************************
 * Navigation data blocks
 *
 *********************************************************************************************************************/

/*
 * Attitude & heading data block (bit 0)
 */
struct AttitudeHeading  //: public NavigationDataBlock
{
  float heading;
  float roll;
  float pitch;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const AttitudeHeading&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const AttitudeHeading& d)
{
  os << "Heading: " << d.heading << std::endl
     << "Roll: " << d.roll << std::endl
     << "Pitch: " << d.pitch;
  return os;
}

/*
 * Attitude & heading standard deviation data block (bit 1)
 */
struct AttitudeHeadingStdDev
{
  float heading_std_dev;
  float roll_std_dev;
  float pitch_std_dev;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const AttitudeHeadingStdDev&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const AttitudeHeadingStdDev& d)
{
  os << "Heading std. dev.: " << d.heading_std_dev << std::endl
     << "Roll std. dev.: " << d.roll_std_dev << std::endl
     << "Pitch std. dev.: " << d.pitch_std_dev;
  return os;
}

/*
 * Heave,surge,sway data block(bit 2)
 */
struct HeaveSurgeSway
{
  float heave_wo_lever_arm;
  float heave;
  float surge;
  float sway;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const HeaveSurgeSway&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const HeaveSurgeSway& d)
{
  os << "Heave wo lever arm: " << d.heave_wo_lever_arm << std::endl
     << "Heave: " << d.heave << std::endl
     << "Surge: " << d.surge << std::endl
     << "Sway: " << d.sway;
  return os;
}

/*
 * Smart Heave data block(bit 3)
 */
struct SmartHeave
{
  std::uint32_t time;
  float smart_heave;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::uint32_t), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const SmartHeave&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const SmartHeave& d)
{
  os << "Time: " << d.time << std::endl
     << "Smart heave: " << d.smart_heave;
  return os;
}

/*
 * Heading, roll, pitch rate data block (bit 4)
 */
struct HeadingRollPitchRate
{
  float heading_rate;
  float roll_rate;
  float pitch_rate;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const HeadingRollPitchRate&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const HeadingRollPitchRate& d)
{
  os << "Heading rate: " << d.heading_rate << std::endl
     << "Roll rate: " << d.roll_rate << std::endl
     << "Pitch rate: " << d.pitch_rate;
  return os;
}

/*
 * Body rotation rates data block in vessel frame (bit 5)
 */
struct RotationRate
{
  float rot_rate_xv1;
  float rot_rate_xv2;
  float rot_rate_xv3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const RotationRate&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const RotationRate& d)
{
  os << "Rotation rate xv1: " << d.rot_rate_xv1 << std::endl
     << "Rotation rate xv2: " << d.rot_rate_xv2 << std::endl
     << "Rotation rate xv3: " << d.rot_rate_xv3;
  return os;
}

/*
 * Accelerations in vessel frame data block (bit 6)
 */
struct AccelerationV
{
  float acc_xv1;
  float acc_xv2;
  float acc_xv3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const AccelerationV&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const AccelerationV& d)
{
  os << "Acceleration xv1: " << d.acc_xv1 << std::endl
     << "Acceleration xv2: " << d.acc_xv2 << std::endl
     << "Acceleration xv3: " << d.acc_xv3;
  return os;
}

/*
 * Position data block (bit 7)
 */
struct Position
{
  double latitude;
  double longitude;
  std::uint8_t altitude_reference;
  float altitude;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(double), sizeof(double), sizeof(std::uint8_t), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const Position&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const Position& d)
{
  os << "Latitude: " << d.latitude << std::endl
     << "Longitude: " << d.longitude << std::endl
     << "Altitude reference: " << static_cast<int>(d.altitude_reference) << std::endl
     << "Altitude: " << d.altitude;
  return os;
}

/*
 * Position standard deviation data block (bit 8)
 */
struct PositionStdDev
{
  float north_std_dev;
  float east_std_dev;
  float north_east_correlation;
  float altitude_std_dev;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const PositionStdDev&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const PositionStdDev& d)
{
  os << "North std. dev.: " << d.north_std_dev << std::endl
     << "East std. dev.: " << d.east_std_dev << std::endl
     << "North east correlation: " << d.north_east_correlation << std::endl
     << "Altitude std. dev.: " << d.altitude_std_dev;
  return os;
}

/*
 * Speed in geographic frame data block (bit 9)
 */
struct VelocityG
{
  float vel_north;
  float vel_east;
  float vel_up;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const VelocityG&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const VelocityG& d)
{
  os << "Velocity north: " << d.vel_north << std::endl
     << "Velocity east: " << d.vel_east << std::endl
     << "Velocity up: " << d.vel_up;
  return os;
}

/*
 * Speed standard deviation in geographic frame data block (bit 10)
 */
struct VelocityGStdDev
{
  float vel_north_std_dev;
  float vel_east_std_dev;
  float vel_up_std_dev;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const VelocityGStdDev&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const VelocityGStdDev& d)
{
  os << "Velocity north std. dev.: " << d.vel_north_std_dev << std::endl
     << "Velocity east std. dev.: " << d.vel_east_std_dev << std::endl
     << "Velocity up std. dev.: " << d.vel_up_std_dev;
  return os;
}

/*
 * Estimated current in geographic frame data block (bit 11)
 */
struct Current
{
  float current_north;
  float current_east;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const Current&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const Current& d)
{
  os << "Current north: " << d.current_north << std::endl
     << "Current east: " << d.current_east;
  return os;
}

/*
 * Current standard deviation in geographic frame data block (bit 12)
 */
struct CurrentStdDev
{
  float current_north_std_dev;
  float current_east_std_dev;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const CurrentStdDev&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const CurrentStdDev& d)
{
  os << "Current north std. dev.: " << d.current_north_std_dev << std::endl
     << "Current east std. dev.: " << d.current_east_std_dev;
  return os;
}

/*
 * System date data block (bit 13)
 */
struct SystemDate
{
  std::uint8_t day;
  std::uint8_t month;
  std::uint16_t year;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::uint8_t), sizeof(std::uint8_t), sizeof(std::uint16_t) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const SystemDate&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const SystemDate& d)
{
  os << "Day: " << static_cast<int>(d.day) << std::endl
     << "Month: " << static_cast<int>(d.month) << std::endl
     << "Year: " << static_cast<int>(d.year);
  return os;
}

/*
 * Sensor status data block (bit 14)
 */
struct SensorStatus
{
  std::uint32_t status_1;
  std::uint32_t status_2;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::uint32_t), sizeof(std::uint32_t) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const SensorStatus&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const SensorStatus& d)
{
  os << "Sensor status 1: " << std::bitset<32>(d.status_1) << std::endl
     << "Sensor status 2: " << std::bitset<32>(d.status_2);
  return os;
}

/*
 * INS Algorithm status data block (bit 15)
 */
struct InsAlgorithmStatus
{
  std::uint32_t status_1;
  std::uint32_t status_2;
  std::uint32_t status_3;
  std::uint32_t status_4;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::uint32_t), sizeof(std::uint32_t), sizeof(std::uint32_t), sizeof(std::uint32_t) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const InsAlgorithmStatus&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const InsAlgorithmStatus& d)
{
  os << "INS algorithm status 1: " << std::bitset<32>(d.status_1) << std::endl
     << "INS algorithm status 2: " << std::bitset<32>(d.status_2) << std::endl
     << "INS algorithm status 3: " << std::bitset<32>(d.status_3) << std::endl
     << "INS algorithm status 4: " << std::bitset<32>(d.status_4);
  return os;
}

/*
 * INS System status data block (bit 16)
 */
struct InsSystemStatus
{
  std::uint32_t status_1;
  std::uint32_t status_2;
  std::uint32_t status_3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::uint32_t), sizeof(std::uint32_t), sizeof(std::uint32_t) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const InsSystemStatus&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const InsSystemStatus& d)
{
  os << "INS system status 1: " << std::bitset<32>(d.status_1) << std::endl
     << "INS system status 2: " << std::bitset<32>(d.status_2) << std::endl
     << "INS system status 3: " << std::bitset<32>(d.status_3);
  return os;
}

/*
 * INS User status data block (bit 17)
 */
struct InsUserStatus
{
  std::uint32_t status;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::uint32_t) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const InsUserStatus&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const InsUserStatus& d)
{
  os << "INS user status: " << std::bitset<32>(d.status);
  return os;
}

/*
 * QUADRANS Algorithm status data block (bit 18)
 */
struct QuadransAlgorithmStatus
{
  std::uint32_t status;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::uint32_t) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const QuadransAlgorithmStatus&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const QuadransAlgorithmStatus& d)
{
  os << "Quadrans algorithm status: " << std::bitset<32>(d.status);
  return os;
}

/*
 * QUADRANS System status data block (bit 19)
 */
struct QuadransSystemStatus
{
  std::uint32_t status_1;
  std::uint32_t status_2;
  std::uint32_t status_3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::uint32_t), sizeof(std::uint32_t), sizeof(std::uint32_t) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const QuadransSystemStatus&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const QuadransSystemStatus& d)
{
  os << "Quadrans system status 1: " << std::bitset<32>(d.status_1) << std::endl
     << "Quadrans system status 2: " << std::bitset<32>(d.status_2) << std::endl
     << "Quadrans system status 3: " << std::bitset<32>(d.status_3);
  return os;
}

/*
 * Quadrans User status data block (bit 20)
 */
struct QuadransUserStatus
{
  std::uint32_t status;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::uint32_t) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const QuadransUserStatus&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const QuadransUserStatus& d)
{
  os << "Quadrans user status: " << std::bitset<32>(d.status);
  return os;
}

/*
 * Heave,surge,sway speed data block(bit 21)
 */
struct HeaveSurgeSwaySpeed
{
  float heave_speed;
  float surge_speed;
  float sway_speed;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const HeaveSurgeSwaySpeed&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const HeaveSurgeSwaySpeed& d)
{
  os << "Heave speed: " << d.heave_speed << std::endl
     << "Surge speed: " << d.surge_speed << std::endl
     << "Sway speed: " << d.sway_speed;
  return os;
}

/*
 * Speed in vessel frame data block(bit 22)
 */
struct VelocityV
{
  float vel_xv1;
  float vel_xv2;
  float vel_xv3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const VelocityV&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const VelocityV& d)
{
  os << "Velocity xv1: " << d.vel_xv1 << std::endl
     << "Velocity xv2: " << d.vel_xv2 << std::endl
     << "Velocity xv3: " << d.vel_xv3;
  return os;
}

/*
 * Acceleration data in geographic frame data block(bit 23)
 */
struct AccelerationG
{
  float acc_north;
  float acc_east;
  float acc_vertical;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const AccelerationG&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const AccelerationG& d)
{
  os << "Acceleration north: " << d.acc_north << std::endl
     << "Acceleration east: " << d.acc_east << std::endl
     << "Acceleration vertical: " << d.acc_vertical;
  return os;
}

/*
 * Course and speed over ground data block(bit 24)
 */
struct CourseSpeedOverGround
{
  float course;
  float speed;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const CourseSpeedOverGround&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const CourseSpeedOverGround& d)
{
  os << "Course over ground: " << d.course << std::endl
     << "Speed over ground: " << d.speed;
  return os;
}

/*
 * Temperatures data block(bit 25)
 */
struct Temperatures
{
  float avg_temp_fog;
  float avg_temp_acc;
  float sensor_board_temp;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const Temperatures&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const Temperatures& d)
{
  os << "Avg. temperature fog: " << d.avg_temp_fog << std::endl
     << "Avg. temperature acc: " << d.avg_temp_acc << std::endl
     << "Sensor board temperature: " << d.sensor_board_temp;
  return os;
}

/*
 * Attitude quaternion data block(bit 26)
 */
struct AttitudeQuaternion
{
  float q0;
  float q1;
  float q2;
  float q3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const AttitudeQuaternion&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const AttitudeQuaternion& d)
{
  os << "Attitude quaternion q0: " << d.q0 << std::endl
     << "Attitude quaternion q1: " << d.q1 << std::endl
     << "Attitude quaternion q2: " << d.q2 << std::endl
     << "Attitude quaternion q3: " << d.q3;
  return os;
}

/*
 * Attitude quaternion standard deviation data block(bit 27)
 */
struct AttitudeQuaternionStdDev
{
  float c1;
  float c2;
  float c3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const AttitudeQuaternionStdDev&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const AttitudeQuaternionStdDev& d)
{
  os << "Attitude quat. std. dev. c1: " << d.c1 << std::endl
     << "Attitude quat. std. dev. c2: " << d.c2 << std::endl
     << "Attitude quat. std. dev. c3: " << d.c3;
  return os;
}

/*
 * Raw acceleration in vessel frame data block(bit 28)
 */
struct RawAccelerationV
{
  float xv1;
  float xv2;
  float xv3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const RawAccelerationV&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const RawAccelerationV& d)
{
  os << "Raw acceleration xv1: " << d.xv1 << std::endl
     << "Raw acceleration xv2: " << d.xv2 << std::endl
     << "Raw acceleration xv3: " << d.xv3;
  return os;
}

/*
 * Acceleration standard deviation in vessel frame data block(bit 29)
 */
struct AccelerationVStdDev
{
  float xv1;
  float xv2;
  float xv3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const AccelerationVStdDev&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const AccelerationVStdDev& d)
{
  os << "Acceleration std. dev. xv1: " << d.xv1 << std::endl
     << "Acceleration std. dev. xv2: " << d.xv2 << std::endl
     << "Acceleration std. dev. xv3: " << d.xv3;
  return os;
}

/*
 * Rotation rate standard deviation in vessel frame data block (bit 30)
 */
struct RotationRateStdDev
{
  float xv1;
  float xv2;
  float xv3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const RotationRateStdDev&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const RotationRateStdDev& d)
{
  os << "Rotation rate std. dev. xv1: " << d.xv1 << std::endl
     << "Rotation rate std. dev. xv2: " << d.xv2 << std::endl
     << "Rotation rate std. dev. xv3: " << d.xv3;
  return os;
}

/**********************************************************************************************************************
 * Extended Navigation data blocks
 *
 *********************************************************************************************************************/

/*
 * Rotation accelerations in vessel frame data block (bit 0)
 */
struct RotationAccelerations
{
  float xv1;
  float xv2;
  float xv3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const RotationAccelerations&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const RotationAccelerations& d)
{
  os << "Rotation acceleration xv1: " << d.xv1 << std::endl
     << "Rotation acceleration xv2: " << d.xv2 << std::endl
     << "Rotation acceleration xv3: " << d.xv3;
  return os;
}

/*
 * Rotation accelerations std_dev in vessel frame data block (bit 1)
 */
struct RotationAccelerationsStdDev
{
  float xv1;
  float xv2;
  float xv3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const RotationAccelerationsStdDev&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const RotationAccelerationsStdDev& d)
{
  os << "Rotation acceleration std. dev. xv1: " << d.xv1 << std::endl
     << "Rotation acceleration std. dev. xv2: " << d.xv2 << std::endl
     << "Rotation acceleration std. dev. xv3: " << d.xv3;
  return os;
}

/*
 * Raw rotation rate in vessel frame data block (bit 2)
 */
struct RawRotationRate
{
  float xv1;
  float xv2;
  float xv3;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(float), sizeof(float), sizeof(float) };
    return sizes;
  }

  friend std::ostream& operator<<(std::ostream&, const RawRotationRate&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const RawRotationRate& d)
{
  os << "Raw rotation rate xv1: " << d.xv1 << std::endl
     << "Raw rotation rate xv2: " << d.xv2 << std::endl
     << "Raw rotation rate xv3: " << d.xv3;
  return os;
}

/**********************************************************************************************************************
 * External sensors data blocks
 *
 *********************************************************************************************************************/

/*
 * UTC data block (bit 0)
 */
struct UTC
{
  std::uint32_t data_validity_time;
  std::uint8_t utc_source;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::uint32_t), sizeof(std::uint8_t) };
    return sizes;
  }

  static const std::string get_data_type()
  {
    return std::string("UTC");
  }

  static const std::vector<std::uint32_t> id_masks()
  {
    const std::vector<std::uint32_t> masks = { 0x00000001 };
    return masks;
  }

  friend std::ostream& operator<<(std::ostream&, const UTC&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const UTC& d)
{
  os << "Data validity time: " << d.data_validity_time << std::endl
     << "UTC source: " << static_cast<int>(d.utc_source);
  return os;
}

/*
 * GPS1, GPS2, Manual GPS data blocks data block (bits 1, 2, 3)
 */
struct GPS
{
  std::int32_t data_validity_time;
  std::uint8_t id;
  std::uint8_t quality;
  double latitude;
  double longitude;
  float altitude;
  float latitude_std_dev;
  float longitude_std_dev;
  float altitude_std_dev;
  float lat_lon_covariance;
  float geoidal_separation;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::int32_t), sizeof(std::uint8_t), sizeof(std::uint8_t), sizeof(double),
                                         sizeof(double),  sizeof(float),   sizeof(float),   sizeof(float),
                                         sizeof(float),   sizeof(float),   sizeof(float) };
    return sizes;
  }

  static const std::string get_data_type()
  {
    return std::string("GPS");
  }

  static const std::vector<std::uint32_t> id_masks()
  {
    const std::vector<std::uint32_t> masks = { 0x00000002, 0x00000004, 0x00000008 };
    return masks;
  }

  friend std::ostream& operator<<(std::ostream&, const GPS&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const GPS& d)
{
  os << "Data validity time: " << d.data_validity_time << std::endl
     << "ID: " << static_cast<int>(d.id) << std::endl
     << "Quality: " << static_cast<int>(d.quality) << std::endl
     << "Latitude: " << d.latitude << std::endl
     << "Longitude: " << d.longitude << std::endl
     << "Altitude: " << d.altitude << std::endl
     << "Latitude std. dev.: " << d.latitude_std_dev << std::endl
     << "Longitude std. dev.: " << d.longitude_std_dev << std::endl
     << "Altitude std. dev.: " << d.altitude_std_dev << std::endl
     << "Latitude longitude covariance: " << d.lat_lon_covariance << std::endl
     << "Geoidal separation: " << d.geoidal_separation;
  return os;
}

/*
 * EMLOG1, EMLOG2 data blocks data block (bits 4, 5)
 */
struct EMLOG
{
  std::int32_t data_validity_time;
  std::uint8_t id;
  float water_speed_xv1;
  float speed_std_dev_xv1;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::int32_t), sizeof(std::uint8_t), sizeof(float), sizeof(float) };
    return sizes;
  }

  static const std::string get_data_type()
  {
    return std::string("EMLOG");
  }

  static const std::vector<std::uint32_t> id_masks()
  {
    const std::vector<std::uint32_t> masks = { 0x00000010, 0x00000020 };
    return masks;
  }

  friend std::ostream& operator<<(std::ostream&, const EMLOG&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const EMLOG& d)
{
  os << "Data validity time: " << d.data_validity_time << std::endl
     << "ID: " << static_cast<int>(d.id) << std::endl
     << "Water speed xv1: " << d.water_speed_xv1 << std::endl
     << "Speed std. dev. xv1: " << d.speed_std_dev_xv1;
  return os;
}

/*
 * USBL1, USBL2, USBL3, data blocks data block (bits 6, 7, 8)
 */
struct USBL
{
  std::int32_t data_validity_time;
  std::uint8_t id;
  std::uint8_t beacon_id[8] = { 0 };
  double latitude;
  double longitude;
  float altitude;
  float north_std_dev;
  float east_std_dev;
  float lat_lon_covariance;
  float altitude_std_dev;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::int32_t), sizeof(std::uint8_t), 8 * sizeof(std::uint8_t), sizeof(double),
                                         sizeof(double),  sizeof(float),   sizeof(float),       sizeof(float),
                                         sizeof(float),   sizeof(float) };
    return sizes;
  }

  static const std::string get_data_type()
  {
    return std::string("USBL");
  }

  static const std::vector<std::uint32_t> id_masks()
  {
    const std::vector<std::uint32_t> masks = { 0x00000040, 0x00000080, 0x00000100 };
    return masks;
  }

  friend std::ostream& operator<<(std::ostream&, const USBL&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const USBL& d)
{
  os << "Data validity time: " << d.data_validity_time << std::endl
     << "ID: " << static_cast<int>(d.id) << std::endl
     << "Beacon ID: [" << d.beacon_id[0] << ", "
                       << d.beacon_id[1] << ", "
                       << d.beacon_id[2] << ", "
                       << d.beacon_id[3] << ", "
                       << d.beacon_id[4] << ", "
                       << d.beacon_id[5] << ", "
                       << d.beacon_id[6] << ", "
                       << d.beacon_id[7] << "]" << std::endl
     << "Latitude: " << d.latitude << std::endl
     << "Longitude: " << d.longitude << std::endl
     << "Altitude: " << d.altitude << std::endl
     << "North std. dev.: " << d.north_std_dev << std::endl
     << "East std. dev.: " << d.east_std_dev << std::endl
     << "Latitude longitude covariance: " << d.lat_lon_covariance << std::endl
     << "Altitude std. dev.: " << d.altitude_std_dev;
  return os;
}

/*
 * Depth data block (bit 9)
 */
struct Depth
{
  std::int32_t data_validity_time;
  float depth;
  float depth_std;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::int32_t), sizeof(float), sizeof(float) };
    return sizes;
  }

  static const std::string get_data_type()
  {
    return std::string("Depth");
  }

  static const std::vector<std::uint32_t> id_masks()
  {
    const std::vector<std::uint32_t> masks = { 0x00000200 };
    return masks;
  }

  friend std::ostream& operator<<(std::ostream&, const Depth&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const Depth& d)
{
  os << "Data validity time: " << d.data_validity_time << std::endl
     << "Depth: " << d.depth << std::endl
     << "Depth std. dev.: " << d.depth_std;
  return os;
}

/*
 * DVL1, DVL2 Ground speed data block (bit 10, 21)
 */
struct DVLGround
{
  std::int32_t data_validity_time;
  std::uint8_t id;
  float xv1_ground_speed;
  float xv2_ground_speed;
  float xv3_ground_speed;
  float speed_of_sound;
  float bottom_range;
  float xv1_speed_std_dev;
  float xv2_speed_std_dev;
  float xv3_speed_std_dev;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = {
      sizeof(std::int32_t), sizeof(std::uint8_t), sizeof(float), sizeof(float), sizeof(float),
      sizeof(float),   sizeof(float),   sizeof(float), sizeof(float), sizeof(float)
    };
    return sizes;
  }

  static const std::string get_data_type()
  {
    return std::string("DVLGround");
  }

  static const std::vector<std::uint32_t> id_masks()
  {
    const std::vector<std::uint32_t> masks = { 0x00000400, 0x00200000 };
    return masks;
  }

  friend std::ostream& operator<<(std::ostream&, const DVLGround&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const DVLGround& d)
{
  os << "Data validity time: " << d.data_validity_time << std::endl
     << "ID: " << static_cast<int>(d.id) << std::endl
     << "Ground speed xv1: " << d.xv1_ground_speed << std::endl
     << "Ground speed xv2: " << d.xv2_ground_speed << std::endl
     << "Ground speed xv3: " << d.xv3_ground_speed << std::endl
     << "Speed of sound: " << d.speed_of_sound << std::endl
     << "Bottom range: " << d.bottom_range << std::endl
     << "Speed std. dev. xv1: " << d.xv1_speed_std_dev << std::endl
     << "Speed std. dev. xv2: " << d.xv2_speed_std_dev << std::endl
     << "Speed std. dev. xv3: " << d.xv3_speed_std_dev;
  return os;
}

/*
 * DVL1, DVL2 Water speed data block (bit 11, 22)
 */
struct DVLWater
{
  std::int32_t data_validity_time;
  std::uint8_t id;
  float xv1_water_speed;
  float xv2_water_speed;
  float xv3_water_speed;
  float speed_of_sound;
  float xv1_speed_std_dev;
  float xv2_speed_std_dev;
  float xv3_speed_std_dev;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::int32_t), sizeof(std::uint8_t), sizeof(float), sizeof(float), sizeof(float),
                                         sizeof(float),   sizeof(float),   sizeof(float), sizeof(float) };
    return sizes;
  }

  static const std::string get_data_type()
  {
    return std::string("DVLWater");
  }

  static const std::vector<std::uint32_t> id_masks()
  {
    const std::vector<std::uint32_t> masks = { 0x00000800, 0x00400000 };
    return masks;
  }

  friend std::ostream& operator<<(std::ostream&, const DVLWater&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const DVLWater& d)
{
  os << "Data validity time: " << d.data_validity_time << std::endl
     << "ID: " << static_cast<int>(d.id) << std::endl
     << "Water speed xv1: " << d.xv1_water_speed << std::endl
     << "Water speed xv2: " << d.xv2_water_speed << std::endl
     << "Water speed xv3: " << d.xv3_water_speed << std::endl
     << "Speed of sound: " << d.speed_of_sound << std::endl
     << "Speed std. dev. xv1: " << d.xv1_speed_std_dev << std::endl
     << "Speed std. dev. xv2: " << d.xv2_speed_std_dev << std::endl
     << "Speed std. dev. xv3: " << d.xv3_speed_std_dev;
  return os;
}

/*
 * External Sound velocity data block (bit 12)
 */
struct ExternalSoundVelocity
{
  std::int32_t data_validity_time;
  float speed_of_sound;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::int32_t), sizeof(float) };
    return sizes;
  }

  static const std::string get_data_type()
  {
    return std::string("ExternalSoundVelocity");
  }

  static const std::vector<std::uint32_t> id_masks()
  {
    const std::vector<std::uint32_t> masks = { 0x00001000 };
    return masks;
  }

  friend std::ostream& operator<<(std::ostream&, const ExternalSoundVelocity&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const ExternalSoundVelocity& d)
{
  os << "Data validity time: " << d.data_validity_time << std::endl
     << "Speed of sound: " << d.speed_of_sound;
  return os;
}

/*
 * DMI data block (bit 13)
 */
struct DMI
{
  std::int32_t data_validity_time;
  std::int32_t pulse_count;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::int32_t), sizeof(std::int32_t) };
    return sizes;
  }

  static const std::string get_data_type()
  {
    return std::string("DMI");
  }

  static const std::vector<std::uint32_t> id_masks()
  {
    const std::vector<std::uint32_t> masks = { 0x00002000 };
    return masks;
  }

  friend std::ostream& operator<<(std::ostream&, const DMI&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const DMI& d)
{
  os << "Data validity time: " << d.data_validity_time << std::endl
     << "Pulse count: " << d.pulse_count;
  return os;
}

/*
 * LBL1, LBL2, LBL3, LBL4 data blocks data block (bits 14, 15, 16, 17)
 */
struct LBL
{
  std::int32_t data_validity_time;
  std::uint8_t id;
  std::uint8_t beacon_id[8];
  double latitude;
  double longitude;
  float altitude;
  float range;
  float range_std_dev;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::int32_t), sizeof(std::uint8_t), 8 * sizeof(std::uint8_t), sizeof(double),
                                         sizeof(double),  sizeof(float),   sizeof(float),       sizeof(float) };
    return sizes;
  }

  static const std::string get_data_type()
  {
    return std::string("LBL");
  }

  static const std::vector<std::uint32_t> id_masks()
  {
    const std::vector<std::uint32_t> masks = { 0x00004000, 0x00008000, 0x00010000, 0x00020000 };
    return masks;
  }

  friend std::ostream& operator<<(std::ostream&, const LBL&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const LBL& d)
{
  os << "Data validity time: " << d.data_validity_time << std::endl
     << "ID: " << static_cast<int>(d.id) << std::endl
     << "Beacon ID: [" << d.beacon_id[0] << ", "
                       << d.beacon_id[1] << ", "
                       << d.beacon_id[2] << ", "
                       << d.beacon_id[3] << ", "
                       << d.beacon_id[4] << ", "
                       << d.beacon_id[5] << ", "
                       << d.beacon_id[6] << ", "
                       << d.beacon_id[7] << "]" << std::endl
     << "Latitude: " << d.latitude << std::endl
     << "Longitude: " << d.longitude << std::endl
     << "Altitude: " << d.altitude << std::endl
     << "Range: " << d.range << std::endl
     << "Range std. dev.: " << d.range_std_dev;
  return os;
}

/*
 * Event marker A,B,C data block (bit 18, 19, 20)
 */
struct EventMarker
{
  std::int32_t data_validity_time;
  std::uint8_t id;
  std::int32_t count;

  static const std::vector<std::size_t> field_sizes()
  {
    const std::vector<std::size_t> sizes = { sizeof(std::int32_t), sizeof(std::uint8_t), sizeof(std::int32_t) };
    return sizes;
  }

  static const std::string get_data_type()
  {
    return std::string("EventMarker");
  }

  static const std::vector<std::uint32_t> id_masks()
  {
    const std::vector<std::uint32_t> masks = { 0x00040000, 0x00080000, 0x00100000 };
    return masks;
  }

  friend std::ostream& operator<<(std::ostream&, const EventMarker&);
} __attribute__((packed));

inline std::ostream& operator<<(std::ostream& os, const EventMarker& d)
{
  os << "Data validity time: " << d.data_validity_time << std::endl
     << "ID: " << static_cast<int>(d.id) << std::endl
     << "Count: " << d.count;
  return os;
}

#endif  // IXBLUE_STDBIN_NAVIGATION_TELEGRAM_HPP
