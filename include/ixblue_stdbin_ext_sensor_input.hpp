/*
 * Copyright (c) 2020 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef IXBLUE_STDBIN_EXT_SENSOR_INPUT_HPP
#define IXBLUE_STDBIN_EXT_SENSOR_INPUT_HPP

#include <cola2_lib/io/tcp_socket.h>

#include <cstdint>
#include <exception>
#include <iostream>
#include <memory>
#include <numeric>
#include <stdexcept>
#include <vector>

#include "ixblue_stdbin_navigation_telegram.hpp"
#include "pod_struct_serialization.hpp"

/*
 * An ExtSensorInput instance is designed to send information of only one external sensor data type following the
 * Ixblue std bin protocol.
 * The format of a telegram sent by an ExtSensorInput will always have a fixed telegram size (given the type) and
 * will be composed of:
 * [InputNavigationHeader] + [ExternalSensorDataBlock] + [Checksum]
 *
 */
template <typename T>
class IxblueStdBinExtSensorInput
{
private:
  // Socket and socket timeout for handling the connection
  std::shared_ptr<cola2::io::TcpSocket> socket_;

  // Identifier for block types that can have repeated inputs
  unsigned int id_;

  // Buffers to store the telegram that is being composed
  cola2::io::DataBuffer telegram_buf_;

public:
  IxblueStdBinExtSensorInput(std::shared_ptr<cola2::io::TcpSocket>, const unsigned int);
  void sendExternalSensorData(T*);
};

template <typename T>
IxblueStdBinExtSensorInput<T>::IxblueStdBinExtSensorInput(std::shared_ptr<cola2::io::TcpSocket> socket,
                                                          const unsigned int id)
  : socket_(socket), id_(id)
{
  // Create input navigation header
  InputNavigationHeader input_nav_header;

  // Compute telegram size
  input_nav_header.telegram_size = sizeof(InputNavigationHeader) + sizeof(T) + sizeof(std::uint32_t);

  // Set external sensor data mask
  const std::vector<std::uint32_t> id_masks = T::id_masks();
  if (id_ < id_masks.size())
  {
    input_nav_header.external_data_mask = id_masks[id_];
  }
  else
  {
    throw std::runtime_error(std::string("Invalid ID (") + std::to_string(id_) +
                             std::string(") for external sensor of type ") + T::get_data_type());
  }

  // Serialize header in advance and copy it to the telegram buffer
  telegram_buf_.resize(input_nav_header.telegram_size);
  serializeOrDeserialize(reinterpret_cast<const unsigned char*>(&input_nav_header),
                         reinterpret_cast<unsigned char*>(telegram_buf_.data()), sizeof(input_nav_header),
                         input_nav_header.field_sizes());
}

template <typename T>
void IxblueStdBinExtSensorInput<T>::sendExternalSensorData(T* data_ptr)
{
  // Serialize body data
  serializeOrDeserialize(reinterpret_cast<const unsigned char*>(data_ptr),
                         reinterpret_cast<unsigned char*>(telegram_buf_.data() + sizeof(InputNavigationHeader)),
                         sizeof(T), T::field_sizes());

  // Compute and serialize checksum
  const std::uint32_t checksum = std::accumulate(telegram_buf_.begin(), telegram_buf_.end() - 4, 0);
  const std::vector<std::size_t> checksum_field_sizes = { sizeof(checksum) };
  serializeOrDeserialize(
      reinterpret_cast<const unsigned char*>(&checksum),
      reinterpret_cast<unsigned char*>(telegram_buf_.data() + sizeof(InputNavigationHeader) + sizeof(T)),
      sizeof(checksum), checksum_field_sizes);

  // Send telegram
  try
  {
    socket_->write(telegram_buf_);
  }
  catch (const std::exception& ex)
  {
    throw std::runtime_error(std::string("Socket error. Could not write external sensor with ID ") +
                             std::to_string(id_) + std::string(" of type ") + T::get_data_type() + std::string(": ") +
                             ex.what());
  }
}

#endif  // IXBLUE_STDBIN_EXT_SENSOR_INPUT_HPP
