/*
 * Copyright (c) 2020 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef IXBLUE_STDBIN_RAW_DATA_HPP
#define IXBLUE_STDBIN_RAW_DATA_HPP

#include "ixblue_stdbin_navigation_telegram.hpp"
#include <numeric>
#include <iostream>

struct IxblueStdBinRawData
{
  // Basic navigation data
  AttitudeHeading attitude_heading;
  AttitudeHeadingStdDev attitude_heading_std_dev;
  HeaveSurgeSway heave_surge_sway;
  SmartHeave smart_heave;
  HeadingRollPitchRate heading_roll_pitch_rate;
  RotationRate rotation_rate;
  AccelerationV acceleration_v;
  Position position;
  PositionStdDev position_std_dev;
  VelocityG velocity_g;
  VelocityGStdDev velocity_g_std_dev;
  Current current;
  CurrentStdDev current_std_dev;
  SystemDate system_date;
  SensorStatus sensor_status;
  InsAlgorithmStatus ins_algorithm_status;
  InsSystemStatus ins_system_status;
  InsUserStatus ins_user_status;
  QuadransAlgorithmStatus quadrans_algorithm_status;
  QuadransSystemStatus quadrans_system_status;
  QuadransUserStatus quadrans_user_status;
  HeaveSurgeSwaySpeed heave_surge_sway_speed;
  VelocityV velocity_v;
  AccelerationG acceleration_g;
  CourseSpeedOverGround course_speed_over_ground;
  Temperatures temperatures;
  AttitudeQuaternion attitude_quaternion;
  AttitudeQuaternionStdDev attitude_quaternion_std_dev;
  RawAccelerationV raw_acceleration_v;
  AccelerationVStdDev acceleration_v_std_dev;
  RotationRateStdDev rotation_rate_std_dev;

  // Extended navigation data
  RotationAccelerations rotation_accelerations;
  RotationAccelerationsStdDev rotation_accelerations_std_dev;
  RawRotationRate raw_rotation_rate;

  // The following has to represent the data members
  static const std::size_t basic_navigation_elements = 31;
  unsigned char* bn_pointers[31];
  std::size_t bn_sizes[31];
  std::vector<std::size_t> bn_field_sizes[31];
  static const std::size_t extended_navigation_elements = 3;
  unsigned char* en_pointers[3];  // Amount of extended navigation data members
  std::size_t en_sizes[3];
  std::vector<std::size_t> en_field_sizes[3];

  // Printing data
  friend std::ostream &operator<<(std::ostream&, const IxblueStdBinRawData&);

  IxblueStdBinRawData():
      bn_pointers{reinterpret_cast<unsigned char*>(&attitude_heading),
                  reinterpret_cast<unsigned char*>(&attitude_heading_std_dev),
                  reinterpret_cast<unsigned char*>(&heave_surge_sway),
                  reinterpret_cast<unsigned char*>(&smart_heave),
                  reinterpret_cast<unsigned char*>(&heading_roll_pitch_rate),
                  reinterpret_cast<unsigned char*>(&rotation_rate),
                  reinterpret_cast<unsigned char*>(&acceleration_v),
                  reinterpret_cast<unsigned char*>(&position),
                  reinterpret_cast<unsigned char*>(&position_std_dev),
                  reinterpret_cast<unsigned char*>(&velocity_g),
                  reinterpret_cast<unsigned char*>(&velocity_g_std_dev),
                  reinterpret_cast<unsigned char*>(&current),
                  reinterpret_cast<unsigned char*>(&current_std_dev),
                  reinterpret_cast<unsigned char*>(&system_date),
                  reinterpret_cast<unsigned char*>(&sensor_status),
                  reinterpret_cast<unsigned char*>(&ins_algorithm_status),
                  reinterpret_cast<unsigned char*>(&ins_system_status),
                  reinterpret_cast<unsigned char*>(&ins_user_status),
                  reinterpret_cast<unsigned char*>(&quadrans_algorithm_status),
                  reinterpret_cast<unsigned char*>(&quadrans_system_status),
                  reinterpret_cast<unsigned char*>(&quadrans_user_status),
                  reinterpret_cast<unsigned char*>(&heave_surge_sway_speed),
                  reinterpret_cast<unsigned char*>(&velocity_v),
                  reinterpret_cast<unsigned char*>(&acceleration_g),
                  reinterpret_cast<unsigned char*>(&course_speed_over_ground),
                  reinterpret_cast<unsigned char*>(&temperatures),
                  reinterpret_cast<unsigned char*>(&attitude_quaternion),
                  reinterpret_cast<unsigned char*>(&attitude_quaternion_std_dev),
                  reinterpret_cast<unsigned char*>(&raw_acceleration_v),
                  reinterpret_cast<unsigned char*>(&acceleration_v_std_dev),
                  reinterpret_cast<unsigned char*>(&rotation_rate_std_dev)},
      bn_sizes{sizeof(attitude_heading),
               sizeof(attitude_heading_std_dev),
               sizeof(heave_surge_sway),
               sizeof(smart_heave),
               sizeof(heading_roll_pitch_rate),
               sizeof(rotation_rate),
               sizeof(acceleration_v),
               sizeof(position),
               sizeof(position_std_dev),
               sizeof(velocity_g),
               sizeof(velocity_g_std_dev),
               sizeof(current),
               sizeof(current_std_dev),
               sizeof(system_date),
               sizeof(sensor_status),
               sizeof(ins_algorithm_status),
               sizeof(ins_system_status),
               sizeof(ins_user_status),
               sizeof(quadrans_algorithm_status),
               sizeof(quadrans_system_status),
               sizeof(quadrans_user_status),
               sizeof(heave_surge_sway_speed),
               sizeof(velocity_v),
               sizeof(acceleration_g),
               sizeof(course_speed_over_ground),
               sizeof(temperatures),
               sizeof(attitude_quaternion),
               sizeof(attitude_quaternion_std_dev),
               sizeof(raw_acceleration_v),
               sizeof(acceleration_v_std_dev),
               sizeof(rotation_rate_std_dev)},
      bn_field_sizes{attitude_heading.field_sizes(),
                     attitude_heading_std_dev.field_sizes(),
                     heave_surge_sway.field_sizes(),
                     smart_heave.field_sizes(),
                     heading_roll_pitch_rate.field_sizes(),
                     rotation_rate.field_sizes(),
                     acceleration_v.field_sizes(),
                     position.field_sizes(),
                     position_std_dev.field_sizes(),
                     velocity_g.field_sizes(),
                     velocity_g_std_dev.field_sizes(),
                     current.field_sizes(),
                     current_std_dev.field_sizes(),
                     system_date.field_sizes(),
                     sensor_status.field_sizes(),
                     ins_algorithm_status.field_sizes(),
                     ins_system_status.field_sizes(),
                     ins_user_status.field_sizes(),
                     quadrans_algorithm_status.field_sizes(),
                     quadrans_system_status.field_sizes(),
                     quadrans_user_status.field_sizes(),
                     heave_surge_sway_speed.field_sizes(),
                     velocity_v.field_sizes(),
                     acceleration_g.field_sizes(),
                     course_speed_over_ground.field_sizes(),
                     temperatures.field_sizes(),
                     attitude_quaternion.field_sizes(),
                     attitude_quaternion_std_dev.field_sizes(),
                     raw_acceleration_v.field_sizes(),
                     acceleration_v_std_dev.field_sizes(),
                     rotation_rate_std_dev.field_sizes()},
      en_pointers{reinterpret_cast<unsigned char*>(&rotation_accelerations),
                  reinterpret_cast<unsigned char*>(&rotation_accelerations_std_dev),
                  reinterpret_cast<unsigned char*>(&raw_rotation_rate)},
      en_sizes{sizeof(rotation_accelerations),
               sizeof(rotation_accelerations_std_dev),
               sizeof(raw_rotation_rate)},
      en_field_sizes{rotation_accelerations.field_sizes(),
                     rotation_accelerations_std_dev.field_sizes(),
                     raw_rotation_rate.field_sizes()}
  {
    // Check sizes of members
    for (std::size_t i = 0; i < 31; ++i)
    {
      std::vector<std::size_t> sizes = bn_field_sizes[i];
      std::size_t sum = std::accumulate(sizes.begin(), sizes.end(), 0);
      if (sum != bn_sizes[i])
        std::cerr << "Size mismatch in basic navigation member " << i << std::endl;
    }
    for (std::size_t i = 0; i < 3; ++i)
    {
      std::vector<std::size_t> sizes = en_field_sizes[i];
      std::size_t sum = std::accumulate(sizes.begin(), sizes.end(), 0);
      if (sum != en_sizes[i])
        std::cerr << "Size mismatch in extended navigation member " << i << std::endl;
    }
  }
};

// Each structure should know how to print itself
inline std::ostream &operator<<(std::ostream& os, const IxblueStdBinRawData& d)
{
  os << "-- RAW DATA ----------------" << std::endl
     << d.attitude_heading << std::endl
     << d.attitude_heading_std_dev << std::endl
     << d.heave_surge_sway << std::endl
     << d.smart_heave << std::endl
     << d.heading_roll_pitch_rate << std::endl
     << d.rotation_rate << std::endl
     << d.acceleration_v << std::endl
     << d.position << std::endl
     << d.position_std_dev << std::endl
     << d.velocity_g << std::endl
     << d.velocity_g_std_dev << std::endl
     << d.current << std::endl
     << d.current_std_dev << std::endl
     << d.system_date << std::endl
     << d.sensor_status << std::endl
     << d.ins_algorithm_status << std::endl
     << d.ins_system_status << std::endl
     << d.ins_user_status << std::endl
     << d.quadrans_algorithm_status << std::endl
     << d.quadrans_system_status << std::endl
     << d.quadrans_user_status << std::endl
     << d.heave_surge_sway_speed << std::endl
     << d.velocity_v << std::endl
     << d.acceleration_g << std::endl
     << d.course_speed_over_ground << std::endl
     << d.temperatures << std::endl
     << d.attitude_quaternion << std::endl
     << d.attitude_quaternion_std_dev << std::endl
     << d.raw_acceleration_v << std::endl
     << d.acceleration_v_std_dev << std::endl
     << d.rotation_rate_std_dev << std::endl
     << d.rotation_accelerations << std::endl
     << d.rotation_accelerations_std_dev << std::endl
     << d.raw_rotation_rate << std::endl
     << "----------------------------";
  return os;
}

#endif  // IXBLUE_STDBIN_RAW_DATA_HPP
