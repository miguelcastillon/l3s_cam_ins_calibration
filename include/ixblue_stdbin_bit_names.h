/*
 * Copyright (c) 2020 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef IXBLUE_STDBIN_BIT_NAMES_H
#define IXBLUE_STDBIN_BIT_NAMES_H

#include <string>
#include <utility>

enum class SeverityLevels
{
  Info,
  Warn,
  Error
};

const std::size_t STATUS_BIT_AMOUNT = 32;

namespace SystemStatus1
{
  const std::pair<SeverityLevels, std::string> severity_message_pairs[STATUS_BIT_AMOUNT] =
  {
    {SeverityLevels::Error, "SERIAL_IN_R_ERR"},
    {SeverityLevels::Error, "INPUT_A_ERR"},
    {SeverityLevels::Error, "INPUT_B_ERR"},
    {SeverityLevels::Error, "INPUT_C_ERR"},
    {SeverityLevels::Error, "INPUT_D_ERR"},
    {SeverityLevels::Error, "INPUT_E_ERR"},
    {SeverityLevels::Error, "INPUT_F_ERR"},
    {SeverityLevels::Error, "INPUT_G_ERR"},
    {SeverityLevels::Info, "INPUT_R_ACTIVITY"},
    {SeverityLevels::Info, "INPUT_A_ACTIVITY"},
    {SeverityLevels::Info, "INPUT_B_ACTIVITY"},
    {SeverityLevels::Info, "INPUT_C_ACTIVITY"},
    {SeverityLevels::Info, "INPUT_D_ACTIVITY"},
    {SeverityLevels::Info, "INPUT_E_ACTIVITY"},
    {SeverityLevels::Info, "INPUT_F_ACTIVITY"},
    {SeverityLevels::Info, "INPUT_G_ACTIVITY"},
    {SeverityLevels::Error, "OUTPUT_R_FULL"},
    {SeverityLevels::Error, "OUTPUT_A_FULL"},
    {SeverityLevels::Error, "OUTPUT_B_FULL"},
    {SeverityLevels::Error, "OUTPUT_C_FULL"},
    {SeverityLevels::Error, "OUTPUT_D_FULL"},
    {SeverityLevels::Error, "OUTPUT_E_FULL"},
    {SeverityLevels::Error, "ETHERNET_PORT_FULL"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, "INTERNAL_TIME_USED"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, "ETHERNET_PORT_ACTIVITY"},
    {SeverityLevels::Info, "PULSE_IN_A_ACTIVITY"},
    {SeverityLevels::Info, "PULSE_IN_B_ACTIVITY"},
    {SeverityLevels::Info, "PULSE_IN_C_ACTIVITY"},
    {SeverityLevels::Info, "PULSE_IN_D_ACTIVITY"},
    {SeverityLevels::Info, ""}
  };
  const std::size_t SERIAL_IN_R_ERR = 0;
  const std::size_t INPUT_A_ERR = 1;
  const std::size_t INPUT_B_ERR = 2;
  const std::size_t INPUT_C_ERR = 3;
  const std::size_t INPUT_D_ERR = 4;
  const std::size_t INPUT_E_ERR = 5;
  const std::size_t INPUT_F_ERR = 6;
  const std::size_t INPUT_G_ERR = 7;
  const std::size_t INPUT_R_ACTIVITY = 8;
  const std::size_t INPUT_A_ACTIVITY = 9;
  const std::size_t INPUT_B_ACTIVITY = 10;
  const std::size_t INPUT_C_ACTIVITY = 11;
  const std::size_t INPUT_D_ACTIVITY = 12;
  const std::size_t INPUT_E_ACTIVITY = 13;
  const std::size_t INPUT_F_ACTIVITY = 14;
  const std::size_t INPUT_G_ACTIVITY = 15;
  const std::size_t OUTPUT_R_FULL = 16;
  const std::size_t OUTPUT_A_FULL = 17;
  const std::size_t OUTPUT_B_FULL = 18;
  const std::size_t OUTPUT_C_FULL = 19;
  const std::size_t OUTPUT_D_FULL = 20;
  const std::size_t OUTPUT_E_FULL = 21;
  const std::size_t ETHERNET_PORT_FULL = 22;
  const std::size_t INTERNAL_TIME_USED = 24;
  const std::size_t ETHERNET_PORT_ACTIVITY = 26;
  const std::size_t PULSE_IN_A_ACTIVITY = 27;
  const std::size_t PULSE_IN_B_ACTIVITY = 28;
  const std::size_t PULSE_IN_C_ACTIVITY = 29;
  const std::size_t PULSE_IN_D_ACTIVITY = 30;
}

namespace SystemStatus2
{
  const std::pair<SeverityLevels, std::string> severity_message_pairs[STATUS_BIT_AMOUNT] =
  {
    {SeverityLevels::Info, "DVL_BT_DETECTED"},
    {SeverityLevels::Info, "DVL_WT_DETECTED"},
    {SeverityLevels::Info, "GPS_DETECTED"},
    {SeverityLevels::Info, "GPS2_DETECTED"},
    {SeverityLevels::Info, "USBL_DETECTED"},
    {SeverityLevels::Info, "LBL_DETECTED"},
    {SeverityLevels::Info, "DEPTH_DETECTED"},
    {SeverityLevels::Info, "EMLOG_DETECTED"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, "UTC_DETECTED"},
    {SeverityLevels::Info, "ALTITUDE_DETECTED"},
    {SeverityLevels::Info, "PPS_DETECTED"},
    {SeverityLevels::Info, "ZUP_MODE_ACTIVATED"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, "MANUAL_GPS_DETECTED"},
    {SeverityLevels::Info, "CTD_DETECTED"},
    {SeverityLevels::Warn, "SIMULATION_MODE"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Error, "DSP_INCOMPATIBILITY"},
    {SeverityLevels::Warn, "HEADING_ALERT"},
    {SeverityLevels::Warn, "POSITION_ALERT"},
    {SeverityLevels::Warn, "WAIT_FOR_POSITION"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Warn, "POLAR_MODE"},
    {SeverityLevels::Info, "INTERNAL_LOG"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, "DOV_CORR_DETECTED"},
    {SeverityLevels::Warn, "MPC_OVERLOAD"},
    {SeverityLevels::Error, "POWER_SUPPLY_FAILURE"},
    {SeverityLevels::Error, "RD_MODE"},
    {SeverityLevels::Info, "CONFIGURATION_SAVED"},
    {SeverityLevels::Info, ""}
  };
  const std::size_t DVL_BT_DETECTED = 0;
  const std::size_t DVL_WT_DETECTED = 1;
  const std::size_t GPS_DETECTED = 2;
  const std::size_t GPS2_DETECTED = 3;
  const std::size_t USBL_DETECTED = 4;
  const std::size_t LBL_DETECTED = 5;
  const std::size_t DEPTH_DETECTED = 6;
  const std::size_t EMLOG_DETECTED = 7;
  const std::size_t UTC_DETECTED = 9;
  const std::size_t ALTITUDE_DETECTED = 10;
  const std::size_t PPS_DETECTED = 11;
  const std::size_t ZUP_MODE_ACTIVATED = 12;
  const std::size_t MANUAL_GPS_DETECTED = 14;
  const std::size_t CTD_DETECTED = 15;
  const std::size_t SIMULATION_MODE = 16;
  const std::size_t DSP_INCOMPATIBILITY = 18;
  const std::size_t HEADING_ALERT = 19;
  const std::size_t POSITION_ALERT = 20;
  const std::size_t WAIT_FOR_POSITION = 21;
  const std::size_t POLAR_MODE = 23;
  const std::size_t INTERNAL_LOG = 24;
  const std::size_t DOV_CORR_DETECTED = 26;
  const std::size_t MPC_OVERLOAD = 27;
  const std::size_t POWER_SUPPLY_FAILURE = 28;
  const std::size_t RD_MODE = 29;
  const std::size_t CONFIGURATION_SAVED = 30;
}

namespace SystemStatus3
{
  const std::pair<SeverityLevels, std::string> severity_message_pairs[STATUS_BIT_AMOUNT] =
  {
    {SeverityLevels::Info, "UTC2_DETECTED"},
    {SeverityLevels::Info, "PPS2_DETECTED"},
    {SeverityLevels::Warn, "ADVANCED_FILTERING"},
    {SeverityLevels::Info, "NTP_SYNC_IN_PROGRESS"},
    {SeverityLevels::Info, "NTP_RECEIVED"},
    {SeverityLevels::Info, "NTP_SYNC"},
    {SeverityLevels::Error, "NTP_FAILED"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, "DVL2_BT_DETECTED"},
    {SeverityLevels::Info, "DVL2_WT_DETECTED"},
    {SeverityLevels::Info, "EMLOG2_DETECTED"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""}
  };
  const std::size_t UTC2_DETECTED = 0;
  const std::size_t PPS2_DETECTED = 1;
  const std::size_t ADVANCED_FILTERING = 2;
  const std::size_t NTP_SYNC_IN_PROGRESS = 3;
  const std::size_t NTP_RECEIVED = 4;
  const std::size_t NTP_SYNC = 5;
  const std::size_t NTP_FAILED = 6;
  const std::size_t DVL2_BT_DETECTED = 8;
  const std::size_t DVL2_WT_DETECTED = 9;
  const std::size_t EMLOG2_DETECTED = 10;
}

namespace AlgorithmStatus1
{
  const std::pair<SeverityLevels, std::string> severity_message_pairs[STATUS_BIT_AMOUNT] =
  {
    {SeverityLevels::Info, "NAVIGATION"},
    {SeverityLevels::Warn, "ALIGNMENT"},
    {SeverityLevels::Info, "FINE_ALIGNMENT"},
    {SeverityLevels::Warn, "DEAD_RECKONING"},
    {SeverityLevels::Info, "GPS_ALTITUDE"},
    {SeverityLevels::Info, "DEPTHSENSOR_ALTITUDE"},
    {SeverityLevels::Info, "ZERO_ALTITUDE"},
    {SeverityLevels::Info, "HYDRO_ALTITUDE"},
    {SeverityLevels::Info, "LOG_RECEIVED"},
    {SeverityLevels::Info, "LOG_VALID"},
    {SeverityLevels::Warn, "LOG_WAITING"},
    {SeverityLevels::Error, "LOG_REJECTED"},
    {SeverityLevels::Info, "GPS_RECEIVED"},
    {SeverityLevels::Info, "GPS_VALID"},
    {SeverityLevels::Warn, "GPS_WAITING"},
    {SeverityLevels::Error, "GPS_REJECTED"},
    {SeverityLevels::Info, "USBL_RECEIVED"},
    {SeverityLevels::Info, "USBL_VALID"},
    {SeverityLevels::Warn, "USBL_WAITING"},
    {SeverityLevels::Error, "USBL_REJECTED"},
    {SeverityLevels::Info, "DEPTH_RECEIVED"},
    {SeverityLevels::Info, "DEPTH_VALID"},
    {SeverityLevels::Warn, "DEPTH_WAITING"},
    {SeverityLevels::Error, "DEPTH_REJECTED"},
    {SeverityLevels::Info, "LBL_RECEIVED"},
    {SeverityLevels::Info, "LBL_VALID"},
    {SeverityLevels::Warn, "LBL_WAITING"},
    {SeverityLevels::Error, "LBL_REJECTED"},
    {SeverityLevels::Error, "ALTITUDE_SATURATION"},
    {SeverityLevels::Error, "SPEED_SATURATION"},
    {SeverityLevels::Warn, "INTERPOLATION_MISSED"},
    {SeverityLevels::Warn, "HEAVE_INITIALISATION"}
  };
  const std::size_t NAVIGATION = 0;
  const std::size_t ALIGNMENT = 1;
  const std::size_t FINE_ALIGNMENT = 2;
  const std::size_t DEAD_RECKONING = 3;
  const std::size_t GPS_ALTITUDE = 4;
  const std::size_t DEPTHSENSOR_ALTITUDE = 5;
  const std::size_t ZERO_ALTITUDE = 6;
  const std::size_t HYDRO_ALTITUDE = 7;
  const std::size_t LOG_RECEIVED = 8;
  const std::size_t LOG_VALID = 9;
  const std::size_t LOG_WAITING = 10;
  const std::size_t LOG_REJECTED = 11;
  const std::size_t GPS_RECEIVED = 12;
  const std::size_t GPS_VALID = 13;
  const std::size_t GPS_WAITING = 14;
  const std::size_t GPS_REJECTED = 15;
  const std::size_t USBL_RECEIVED = 16;
  const std::size_t USBL_VALID = 17;
  const std::size_t USBL_WAITING = 18;
  const std::size_t USBL_REJECTED = 19;
  const std::size_t DEPTH_RECEIVED = 20;
  const std::size_t DEPTH_VALID = 21;
  const std::size_t DEPTH_WAITING = 22;
  const std::size_t DEPTH_REJECTED = 23;
  const std::size_t LBL_RECEIVED = 24;
  const std::size_t LBL_VALID = 25;
  const std::size_t LBL_WAITING = 26;
  const std::size_t LBL_REJECTED = 27;
  const std::size_t ALTITUDE_SATURATION = 28;
  const std::size_t SPEED_SATURATION = 29;
  const std::size_t INTERPOLATION_MISSED = 30;
  const std::size_t HEAVE_INITIALISATION = 31;
}

namespace AlgorithmStatus2
{
  const std::pair<SeverityLevels, std::string> severity_message_pairs[STATUS_BIT_AMOUNT] =
  {
    {SeverityLevels::Info, "WATERTRACK_RECEIVED"},
    {SeverityLevels::Info, "WATERTRACK_VALID"},
    {SeverityLevels::Warn, "WATERTRACK_WAITING"},
    {SeverityLevels::Error, "WATERTRACK_REJECTED"},
    {SeverityLevels::Info, "GPS2_RECEIVED"},
    {SeverityLevels::Info, "GPS2_VALID"},
    {SeverityLevels::Warn, "GPS2_WAITING"},
    {SeverityLevels::Error, "GPS2_REJECTED"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, "ALTITUDE_RECEIVED"},
    {SeverityLevels::Info, "ALTITUDE_VALID"},
    {SeverityLevels::Warn, "ALTITUDE_WAITING"},
    {SeverityLevels::Error, "ALTITUDE_REJECTED"},
    {SeverityLevels::Info, "ZUPT_MODE_ACTIVATED"},
    {SeverityLevels::Info, "ZUPT_MODE_VALID"},
    {SeverityLevels::Info, "AUTOSTATICBENCH_ZUPT_MODE"},
    {SeverityLevels::Info, "AUTOSTATICBENCH_ZUPT_VALID"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Warn, "FAST_ALIGNEMENT"},
    {SeverityLevels::Warn, "EMULATION_MODE"},
    {SeverityLevels::Info, "EMLOG_RECEIVED"},
    {SeverityLevels::Info, "EMLOG_VALID"},
    {SeverityLevels::Warn, "EMLOG_WAITING"},
    {SeverityLevels::Error, "EMLOG_REJECTED"},
    {SeverityLevels::Info, "MANUALGPS_RECEIVED"},
    {SeverityLevels::Info, "MANUALGPS_VALID"},
    {SeverityLevels::Warn, "MANUALGPS_WAITING"},
    {SeverityLevels::Error, "MANUALGPS_REJECTED"}
  };
  const std::size_t WATERTRACK_RECEIVED = 0;
  const std::size_t WATERTRACK_VALID = 1;
  const std::size_t WATERTRACK_WAITING = 2;
  const std::size_t WATERTRACK_REJECTED = 3;
  const std::size_t GPS2_RECEIVED = 4;
  const std::size_t GPS2_VALID = 5;
  const std::size_t GPS2_WAITING = 6;
  const std::size_t GPS2_REJECTED = 7;
  const std::size_t ALTITUDE_RECEIVED = 12;
  const std::size_t ALTITUDE_VALID = 13;
  const std::size_t ALTITUDE_WAITING = 14;
  const std::size_t ALTITUDE_REJECTED = 15;
  const std::size_t ZUPT_MODE_ACTIVATED = 16;
  const std::size_t ZUPT_MODE_VALID = 17;
  const std::size_t AUTOSTATICBENCH_ZUPT_MODE = 18;
  const std::size_t AUTOSTATICBENCH_ZUPT_VALID = 19;
  const std::size_t FAST_ALIGNEMENT = 22;
  const std::size_t EMULATION_MODE = 23;
  const std::size_t EMLOG_RECEIVED = 24;
  const std::size_t EMLOG_VALID = 25;
  const std::size_t EMLOG_WAITING = 26;
  const std::size_t EMLOG_REJECTED = 27;
  const std::size_t MANUALGPS_RECEIVED = 28;
  const std::size_t MANUALGPS_VALID = 29;
  const std::size_t MANUALGPS_WAITING = 30;
  const std::size_t MANUALGPS_REJECTED = 31;
}

namespace AlgorithmStatus3
{
  const std::pair<SeverityLevels, std::string> severity_message_pairs[STATUS_BIT_AMOUNT] =
  {
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, "EMLOG2_RECEIVED"},
    {SeverityLevels::Info, "EMLOG2_VALID"},
    {SeverityLevels::Warn, "EMLOG2_WAITING"},
    {SeverityLevels::Error, "EMLOG2_REJECTED"},
    {SeverityLevels::Info, "USBL2_RECEIVED"},
    {SeverityLevels::Info, "USBL2_VALID"},
    {SeverityLevels::Warn, "USBL2_WAITING"},
    {SeverityLevels::Error, "USBL2_REJECTED"},
    {SeverityLevels::Info, "USBL3_RECEIVED"},
    {SeverityLevels::Info, "USBL3_VALID"},
    {SeverityLevels::Warn, "USBL3_WAITING"},
    {SeverityLevels::Error, "USBL3_REJECTED"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Warn, "CALCHK"},
    {SeverityLevels::Error, "RESTORE_ATTITUDE_FAILED"},
    {SeverityLevels::Info, "REL_SPD_ZUP_ACTIVATED"},
    {SeverityLevels::Info, "REL_SPD_ZUP_VALID"},
    {SeverityLevels::Error, "EXT_SENSOR_OUTDATED"},
    {SeverityLevels::Warn, "SENSOR_USED_BEFORE_CALIB"},
    {SeverityLevels::Error, "RESTORE_ATTITUDE_REJECTED"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""}
  };
  const std::size_t EMLOG2_RECEIVED = 4;
  const std::size_t EMLOG2_VALID = 5;
  const std::size_t EMLOG2_WAITING = 6;
  const std::size_t EMLOG2_REJECTED = 7;
  const std::size_t USBL2_RECEIVED = 8;
  const std::size_t USBL2_VALID = 0;
  const std::size_t USBL2_WAITING = 10;
  const std::size_t USBL2_REJECTED = 11;
  const std::size_t USBL3_RECEIVED = 12;
  const std::size_t USBL3_VALID = 13;
  const std::size_t USBL3_WAITING = 14;
  const std::size_t USBL3_REJECTED = 15;
  const std::size_t CALCHK = 17;
  const std::size_t RESTORE_ATTITUDE_FAILED = 18;
  const std::size_t REL_SPD_ZUP_ACTIVATED = 19;
  const std::size_t REL_SPD_ZUP_VALID = 20;
  const std::size_t EXT_SENSOR_OUTDATED = 21;
  const std::size_t SENSOR_USED_BEFORE_CALIB = 22;
  const std::size_t RESTORE_ATTITUDE_REJECTED = 23;
}

namespace AlgorithmStatus4
{
  const std::pair<SeverityLevels, std::string> severity_message_pairs[STATUS_BIT_AMOUNT] =
  {
    {SeverityLevels::Info, "LOG2_RECEIVED"},
    {SeverityLevels::Info, "LOG2_VALID"},
    {SeverityLevels::Warn, "LOG2_WAITING"},
    {SeverityLevels::Error, "LOG2_REJECTED"},
    {SeverityLevels::Info, "WATERTRACK2_RECEIVED"},
    {SeverityLevels::Info, "WATERTRACK2_VALID"},
    {SeverityLevels::Warn, "WATERTRACK2_WAITING"},
    {SeverityLevels::Error, "WATERTRACK2_REJECTED"},
    {SeverityLevels::Info, "DVL_DIST_TRAVELLED_VALID"},
    {SeverityLevels::Info, "DVL_CALIBRATION_NONE"},
    {SeverityLevels::Info, "DVL_ROUGH_CALIBRATION"},
    {SeverityLevels::Info, "DVL_FINE_CALIBRATION"},
    {SeverityLevels::Info, "DVL_CHECK_CALIBRATION"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""}
  };
  const std::size_t LOG2_RECEIVED = 0;
  const std::size_t LOG2_VALID = 1;
  const std::size_t LOG2_WAITING = 2;
  const std::size_t LOG2_REJECTED = 3;
  const std::size_t WATERTRACK2_RECEIVED = 4;
  const std::size_t WATERTRACK2_VALID = 5;
  const std::size_t WATERTRACK2_WAITING = 6;
  const std::size_t WATERTRACK2_REJECTED = 7;
  const std::size_t DVL_DIST_TRAVELLED_VALID = 8;
  const std::size_t DVL_CALIBRATION_NONE = 9;
  const std::size_t DVL_ROUGH_CALIBRATION = 10;
  const std::size_t DVL_FINE_CALIBRATION = 11;
  const std::size_t DVL_CHECK_CALIBRATION = 12;
}

namespace SensorStatus1
{
  const std::pair<SeverityLevels, std::string> severity_message_pairs[STATUS_BIT_AMOUNT] =
  {
    {SeverityLevels::Error, "DATA_READY_ERR"},
    {SeverityLevels::Error, "SOURCE_POWER_CONTROL_ERR"},
    {SeverityLevels::Error, "SOURCE_DIODE_ERR"},
    {SeverityLevels::Error, "SOURCE_MODE_ERR"},
    {SeverityLevels::Error, "ACC_X_SATURATION_ERR"},
    {SeverityLevels::Error, "ACC_Y_SATURATION_ERR"},
    {SeverityLevels::Error, "ACC_Z_SATURATION_ERR"},
    {SeverityLevels::Error, "ACC_X_ACQ_ERR"},
    {SeverityLevels::Error, "ACC_Y_ACQ_ERR"},
    {SeverityLevels::Error, "ACC_Z_ACQ_ERR"},
    {SeverityLevels::Error, "FOG_X_SATURATION_ERR"},
    {SeverityLevels::Error, "FOG_Y_SATURATION_ERR"},
    {SeverityLevels::Error, "FOG_Z_SATURATION_ERR"},
    {SeverityLevels::Error, "FOG_X_VPI_ERR"},
    {SeverityLevels::Error, "FOG_Y_VPI_ERR"},
    {SeverityLevels::Error, "FOG_Z_VPI_ERR"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Error, "FOG_X_ACQ_ERR"},
    {SeverityLevels::Error, "FOG_Y_ACQ_ERR"},
    {SeverityLevels::Error, "FOG_Z_ACQ_ERR"},
    {SeverityLevels::Error, "FOG_X_CRC_ERR"},
    {SeverityLevels::Error, "FOG_Y_CRC_ERR"},
    {SeverityLevels::Error, "FOG_Z_CRC_ERR"},
    {SeverityLevels::Error, "TEMP_ACQ_ERR"},
    {SeverityLevels::Error, "TEMP_THRESHOLD_ERR"},
    {SeverityLevels::Error, "DTEMP_THRESHOLD_ERR"},
    {SeverityLevels::Warn, "SENSOR_DATA_FIFO_WARNING"},
    {SeverityLevels::Error, "SENSOR_DATA_FIFO_ERR"},
    {SeverityLevels::Error, "SOURCE_POWER_ERR"},
    {SeverityLevels::Error, "SOURCE_RECEPTION_ERR"}
  };
  const std::size_t DATA_READY_ERR = 0;
  const std::size_t SOURCE_POWER_CONTROL_ERR = 1;
  const std::size_t SOURCE_DIODE_ERR = 2;
  const std::size_t SOURCE_MODE_ERR = 3;
  const std::size_t ACC_X_SATURATION_ERR = 4;
  const std::size_t ACC_Y_SATURATION_ERR = 5;
  const std::size_t ACC_Z_SATURATION_ERR = 6;
  const std::size_t ACC_X_ACQ_ERR = 7;
  const std::size_t ACC_Y_ACQ_ERR = 8;
  const std::size_t ACC_Z_ACQ_ERR = 9;
  const std::size_t FOG_X_SATURATION_ERR = 10;
  const std::size_t FOG_Y_SATURATION_ERR = 11;
  const std::size_t FOG_Z_SATURATION_ERR = 12;
  const std::size_t FOG_X_VPI_ERR = 13;
  const std::size_t FOG_Y_VPI_ERR = 14;
  const std::size_t FOG_Z_VPI_ERR = 15;
  const std::size_t FOG_X_ACQ_ERR = 19;
  const std::size_t FOG_Y_ACQ_ERR = 20;
  const std::size_t FOG_Z_ACQ_ERR = 21;
  const std::size_t FOG_X_CRC_ERR = 22;
  const std::size_t FOG_Y_CRC_ERR = 23;
  const std::size_t FOG_Z_CRC_ERR = 24;
  const std::size_t TEMP_ACQ_ERR = 25;
  const std::size_t TEMP_THRESHOLD_ERR = 26;
  const std::size_t DTEMP_THRESHOLD_ERR = 27;
  const std::size_t SENSOR_DATA_FIFO_WARNING = 28;
  const std::size_t SENSOR_DATA_FIFO_ERR = 29;
  const std::size_t SOURCE_POWER_ERR = 30;
  const std::size_t SOURCE_RECEPTION_ERR = 31;
}

namespace SensorStatus2
{
  const std::pair<SeverityLevels, std::string> severity_message_pairs[STATUS_BIT_AMOUNT] =
  {
    {SeverityLevels::Error, "FOG_X_ERR"},
    {SeverityLevels::Error, "FOG_Y_ERR"},
    {SeverityLevels::Error, "FOG_Z_ERR"},
    {SeverityLevels::Error, "SOURCE_ERR"},
    {SeverityLevels::Error, "ACC_X_ERR"},
    {SeverityLevels::Error, "ACC_Y_ERR"},
    {SeverityLevels::Error, "ACC_Z_ERR"},
    {SeverityLevels::Error, "TEMP_ERR"},
    {SeverityLevels::Error, "DSP_OVERLOAD"},
    {SeverityLevels::Error, "ERR_INIT_CAN_ACC_X"},
    {SeverityLevels::Error, "ERR_INIT_CAN_ACC_Y"},
    {SeverityLevels::Error, "ERR_INIT_CAN_ACC_Z"},
    {SeverityLevels::Error, "MODELISATION_ERROR"},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Info, ""},
    {SeverityLevels::Error, "DEGRADED_MODE"},
    {SeverityLevels::Error, "FAILURE_MODE"}
  };
  const std::size_t FOG_X_ERR = 0;
  const std::size_t FOG_Y_ERR = 1;
  const std::size_t FOG_Z_ERR = 2;
  const std::size_t SOURCE_ERR = 3;
  const std::size_t ACC_X_ERR = 4;
  const std::size_t ACC_Y_ERR = 5;
  const std::size_t ACC_Z_ERR = 6;
  const std::size_t TEMP_ERR = 7;
  const std::size_t DSP_OVERLOAD = 8;
  const std::size_t ERR_INIT_CAN_ACC_X = 9;
  const std::size_t ERR_INIT_CAN_ACC_Y = 10;
  const std::size_t ERR_INIT_CAN_ACC_Z = 11;
  const std::size_t MODELISATION_ERROR = 12;
  const std::size_t DEGRADED_MODE = 30;
  const std::size_t FAILURE_MODE = 31;
}

namespace UserStatus
{
  const std::pair<SeverityLevels, std::string> severity_message_pairs[STATUS_BIT_AMOUNT] =
  {
    {SeverityLevels::Info, "DVL_RECEIVED_VALID"},
    {SeverityLevels::Info, "GPS_RECEIVED_VALID"},
    {SeverityLevels::Info, "DEPTH_RECEIVED_VALID"},
    {SeverityLevels::Info, "USBL_RECEIVED_VALID"},
    {SeverityLevels::Info, "LBL_RECEIVED_VALID"},
    {SeverityLevels::Info, "GPS2_RECEIVED_VALID"},
    {SeverityLevels::Info, "EMLOG_RECEIVED_VALID"},
    {SeverityLevels::Info, "MANUAL_GPS_RECEIVED_VALID"},
    {SeverityLevels::Info, "TIME_RECEIVED_VALID"},
    {SeverityLevels::Error, "FOG_ANOMALY"},
    {SeverityLevels::Error, "ACC_ANOMALY"},
    {SeverityLevels::Error, "TEMPERATURE_ERR"},
    {SeverityLevels::Error, "CPU_OVERLOAD"},
    {SeverityLevels::Error, "DYNAMIC_EXCEDEED"},
    {SeverityLevels::Error, "SPEED_SATURATION"},
    {SeverityLevels::Error, "ALTITUDE_SATURATION"},
    {SeverityLevels::Error, "INPUT_A_ERR"},
    {SeverityLevels::Error, "INPUT_B_ERR"},
    {SeverityLevels::Error, "INPUT_C_ERR"},
    {SeverityLevels::Error, "INPUT_D_ERR"},
    {SeverityLevels::Error, "INPUT_E_ERR"},
    {SeverityLevels::Error, "OUTPUT_A_ERR"},
    {SeverityLevels::Error, "OUTPUT_B_ERR"},
    {SeverityLevels::Error, "OUTPUT_C_ERR"},
    {SeverityLevels::Error, "OUTPUT_D_ERR"},
    {SeverityLevels::Error, "OUTPUT_E_ERR"},
    {SeverityLevels::Error, "HRP_INVALID"},
    {SeverityLevels::Info, "ALIGNEMENT"},
    {SeverityLevels::Info, "FINE_ALIGNMENT"},
    {SeverityLevels::Info, "NAVIGATION"},
    {SeverityLevels::Error, "DEGRADED_MODE"},
    {SeverityLevels::Error, "FAILURE_MODE"}
  };
  const std::size_t DVL_RECEIVED_VALID = 0;
  const std::size_t GPS_RECEIVED_VALID = 1;
  const std::size_t DEPTH_RECEIVED_VALID = 2;
  const std::size_t USBL_RECEIVED_VALID = 3;
  const std::size_t LBL_RECEIVED_VALID = 4;
  const std::size_t GPS2_RECEIVED_VALID = 5;
  const std::size_t EMLOG_RECEIVED_VALID = 6;
  const std::size_t MANUAL_GPS_RECEIVED_VALID = 7;
  const std::size_t TIME_RECEIVED_VALID = 8;
  const std::size_t FOG_ANOMALY = 9;
  const std::size_t ACC_ANOMALY = 10;
  const std::size_t TEMPERATURE_ERR = 11;
  const std::size_t CPU_OVERLOAD = 12;
  const std::size_t DYNAMIC_EXCEDEED = 13;
  const std::size_t SPEED_SATURATION = 14;
  const std::size_t ALTITUDE_SATURATION = 15;
  const std::size_t INPUT_A_ERR = 16;
  const std::size_t INPUT_B_ERR = 17;
  const std::size_t INPUT_C_ERR = 18;
  const std::size_t INPUT_D_ERR = 19;
  const std::size_t INPUT_E_ERR = 20;
  const std::size_t OUTPUT_A_ERR = 21;
  const std::size_t OUTPUT_B_ERR = 22;
  const std::size_t OUTPUT_C_ERR = 23;
  const std::size_t OUTPUT_D_ERR = 24;
  const std::size_t OUTPUT_E_ERR = 25;
  const std::size_t HRP_INVALID = 26;
  const std::size_t ALIGNEMENT = 27;
  const std::size_t FINE_ALIGNMENT = 28;
  const std::size_t NAVIGATION = 29;
  const std::size_t DEGRADED_MODE = 30;
  const std::size_t FAILURE_MODE = 31;
}

#endif  // IXBLUE_STDBIN_BIT_NAMES_H
