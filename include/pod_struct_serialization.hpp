/*
 * Copyright (c) 2020 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef PODSTRUCT_SERIALIZATION_H
#define PODSTRUCT_SERIALIZATION_H

#include <endian.h>
#include <algorithm>
#include <cstring>

/*
 * Helper function to swap bytes of an array given a data size
 */
inline void swapBytes(unsigned char* data, const std::size_t data_size)
{
  for (std::size_t i = 0, end = data_size / 2; i < end; ++i)
    std::swap(data[i], data[data_size - i - 1]);
}

/*
 * Serialize or deserialize struct. Serialize/deserialize is done in block operations. This can be done provided that:
 * - all the structs are PODs (i.e,. are trivially copyable and follow a standard layout).
 * - struct alignment has been handled on struct declaration (by using __attribute__(packed)).
 * - endianess changes between host machine and network protocol (using big-endian)
 *   are handled by swapping corresponding bytes (floats and doubles are assumed to be IEEE 754).
 */
inline void serializeOrDeserialize(const unsigned char* source, unsigned char* destination, const std::size_t data_size, const std::vector<std::size_t>& field_sizes)
{
  // Copy data from source to destination
  memcpy(destination, source, data_size);

  // Reverse endianess of struct members
#if __BYTE_ORDER == __LITTLE_ENDIAN
  unsigned char* field_ptr = destination;
  for (const auto& field_size : field_sizes)
  {
    swapBytes(field_ptr, field_size);
    field_ptr += field_size;
  }
#endif
}

#endif  // PODSTRUCT_SERIALIZATION_H
