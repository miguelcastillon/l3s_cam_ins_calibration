# OpenMP
find_package(OpenMP)
if(OpenMP_FOUND)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()

# OpenCV
find_package(OpenCV 3.2 REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})
message(STATUS "OpenCV version: ${OpenCV_VERSION}")

# EBUS
set(Ebus_DIR "${CMAKE_CURRENT_LIST_DIR}/")
find_package(Ebus REQUIRED)
include_directories(${Ebus_INCLUDE_DIRS})

# Boost
find_package(Boost REQUIRED)
include_directories(${Boost_INCLUDE_DIR})

# PCL
find_package(PCL 1.8.1 REQUIRED)
include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})
message(STATUS "PCL version: ${PCL_VERSION}")

# VTK
find_package(VTK 6.3 REQUIRED)
include(${VTK_USE_FILE})
message(STATUS "VTK version: ${VTK_MAJOR_VERSION}.${VTK_MINOR_VERSION}")

# Qt5
find_package(Qt5 REQUIRED COMPONENTS Core Gui Widgets UiPlugin)
# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
message(STATUS "Qt5 version: ${Qt5_VERSION}")

# Ceres
find_package(Ceres REQUIRED)
include_directories(${CERES_INCLUDE_DIR})
message(STATUS "Ceres version: ${CERES_VERSION}")

# Tiny XML
find_package(TinyXML REQUIRED)
include_directories(${TinyXML_INCLUDE_DIR})

# Aruco
find_package(aruco REQUIRED)
include_directories(${aruco_INCLUDE_DIRS})
message(STATUS "Aruco version: ${aruco_VERSION}")

# CTIF Xtreme io
include_directories("/opt/xtreme_io/include/")
