#include "readINS.h"
#include <exception>
IxbluePhins::IxbluePhins() : first_config_(true), ned_(0.0, 0.0, 0.0)  // NED has no default constructor...
{
  // Get config
  getConfig();

  // Connect to the INS
  getConnectionConfig();
  while (!configurePhinsIOConnections() && (!ros::isShuttingDown()))
  {
    ROS_ERROR("Impossible to connect to INS ports");
    ros::Duration(1.0).sleep();
  }
  ROS_INFO("INS connections ok");
  initFlagsAndDiagnostics();

  // Show message
  ROS_INFO("Initialized");
}

bool IxbluePhins::configurePhinsIOConnections()
{
  // Flag to track if basic connections have failed
  bool valid_connection = true;

  // Nav output
  cola2::io::TCPConfig tcp_config_nav;
  tcp_config_nav.ip = "192.168.1.82"; //connection_config_.ip;
  std::cout << tcp_config_nav.ip << std::endl;
  tcp_config_nav.port = connection_config_.nav_output_port;
  socket_nav_out_ = std::make_shared<cola2::io::TcpSocket>(tcp_config_nav);
  try
  {
    socket_nav_out_->open();
  }
  catch (const std::exception& ex)
  {
    ROS_WARN_STREAM("Error establishing connection to INS nav output: " << ex.what());
    socket_nav_out_->close();
    valid_connection = false;
  }

  return valid_connection;
}

void IxbluePhins::initFlagsAndDiagnostics()
{
  init_phins_ = false;
  last_phins_data_ = 0.0;
  last_init_phins_time_ = 0.0;
}

// *****************************************************************************
// Updates from PHINS and ROS messages from sensors
// *****************************************************************************
std::vector<double> IxbluePhins::mainCallback()
{
  try
  {
    // Parse telegram
    /*bool read_final = false;
    // Try to read only last element from buffer
    int count = 0;
    IxblueStdBinRawData raw_data_tmp_1;
    IxblueStdBinRawData raw_data_tmp_2;
    IxblueStdBinRawData raw_data_tmp_3;
    while(!read_final)
    {
		IxblueStdBinRawData raw_data_tmp;
		try
		{
			raw_data_tmp = parseNavigationTelegram(socket_nav_out_, connection_config_.socket_timeout);
			if(raw_data_tmp.system_date.year != 2020)
			{
				throw;
			}
		}
		catch (const std::exception& ex)
		{
			// TODO: This wouldn't work if first attempt to read throws an exception
			read_final = true;
			if (count == 0)
			{
				read_final = false;
				count--;
			}
			else if(raw_data_tmp.system_date.year == 2020)
			{
				raw_data_ = raw_data_tmp;
			}
			else if(raw_data_tmp_1.system_date.year == 2020)
			{
				raw_data_ = raw_data_tmp_1;
			}
			else if(raw_data_tmp_2.system_date.year == 2020)
			{
				raw_data_ = raw_data_tmp_2;
			}
			else if(raw_data_tmp_3.system_date.year == 2020)
			{
				raw_data_ = raw_data_tmp_3;
			}
			else
			{
				std::cout << "ERROR: Not read proper navigation data from INS" << std::endl;
				std::cout << "raw_data_tmp = " << raw_data_tmp << std::endl;
				std::cout << std::endl;
				std::cout << "raw_data_tmp_1 = " << raw_data_tmp_1 << std::endl;
				std::cout << std::endl;
				std::cout << "raw_data_tmp_2 = " << raw_data_tmp_2 << std::endl;
				std::cout << std::endl;
				std::cout << "raw_data_tmp_3 = " << raw_data_tmp_3 << std::endl;
				std::cout << std::endl;
			}
		}
		raw_data_tmp_3 = raw_data_tmp_2;
		raw_data_tmp_2 = raw_data_tmp_1;
		raw_data_tmp_1 = raw_data_tmp;
		count++;
	}*/
	// std::cout << "raw_data_ = " << raw_data_ << std::endl;
	
	std::size_t messages_read = 0;
	try
	{
		for (;;)
		{
			raw_data_ = parseNavigationTelegram(socket_nav_out_, connection_config_.socket_timeout);
			++messages_read;
		}
	}	
	catch (...) {}
	if (messages_read == 0)
	    throw std::runtime_error("Unable to read raw data from INS");
	// std::cout << raw_data_ << std::endl;
	std::cout << "Discarding " << messages_read - 1 << " messages" << std::endl;

    // This must be after parse. If parsing fails this will not be executed
    const ros::Time stamp = ros::Time::now();
    last_phins_data_ = stamp.toSec();

    // Check if PHINS is still in coarse alignment phase
    if (!(raw_data_.ins_algorithm_status.status_1 & (1 << AlgorithmStatus1::ALIGNMENT)))
    {
      // If we restart and the phins is aligned, we automatically set the flag of initialized to true
      if (!init_phins_)
      {
        init_phins_ = true;
        last_init_phins_time_ = last_phins_data_;
      }
    }

    // Create an array of tuples with the following elements:
    //  - Name of the status datagram
    //  - Mask of the status datagram
    //  - Pointer to an array of pairs [SeverityLevels, string]
    //  - Vector of bits to check for errors to invalidate the navigation
    const std::array<std::tuple<const std::string, const std::uint32_t, const std::pair<SeverityLevels, std::string>*,
                                const std::vector<std::size_t>>,
                     10>
        status_tuples = {
          { { "SystemStatus1", raw_data_.ins_system_status.status_1, SystemStatus1::severity_message_pairs, {} },
            { "SystemStatus2",
              raw_data_.ins_system_status.status_2,
              SystemStatus2::severity_message_pairs,
              { SystemStatus2::DSP_INCOMPATIBILITY, SystemStatus2::POWER_SUPPLY_FAILURE, SystemStatus2::RD_MODE } },
            { "SystemStatus3", raw_data_.ins_system_status.status_3, SystemStatus3::severity_message_pairs, {} },
            { "AlgorithmStatus1",
              raw_data_.ins_algorithm_status.status_1,
              AlgorithmStatus1::severity_message_pairs,
              { AlgorithmStatus1::ALTITUDE_SATURATION, AlgorithmStatus1::SPEED_SATURATION } },
            { "AlgorithmStatus2",
              raw_data_.ins_algorithm_status.status_2,
              AlgorithmStatus2::severity_message_pairs,
              {} },
            { "AlgorithmStatus3",
              raw_data_.ins_algorithm_status.status_3,
              AlgorithmStatus3::severity_message_pairs,
              { AlgorithmStatus3::RESTORE_ATTITUDE_FAILED, AlgorithmStatus3::RESTORE_ATTITUDE_REJECTED } },
            { "AlgorithmStatus4",
              raw_data_.ins_algorithm_status.status_4,
              AlgorithmStatus4::severity_message_pairs,
              {} },
            { "SensorStatus1",
              raw_data_.sensor_status.status_1,
              SensorStatus1::severity_message_pairs,
              { SensorStatus1::DATA_READY_ERR,       SensorStatus1::SOURCE_POWER_CONTROL_ERR,
                SensorStatus1::SOURCE_DIODE_ERR,     SensorStatus1::SOURCE_MODE_ERR,
                SensorStatus1::ACC_X_SATURATION_ERR, SensorStatus1::ACC_Y_SATURATION_ERR,
                SensorStatus1::ACC_Z_SATURATION_ERR, SensorStatus1::ACC_X_ACQ_ERR,
                SensorStatus1::ACC_Y_ACQ_ERR,        SensorStatus1::ACC_Z_ACQ_ERR,
                SensorStatus1::FOG_X_SATURATION_ERR, SensorStatus1::FOG_Y_SATURATION_ERR,
                SensorStatus1::FOG_Z_SATURATION_ERR, SensorStatus1::FOG_X_VPI_ERR,
                SensorStatus1::FOG_Y_VPI_ERR,        SensorStatus1::FOG_Z_VPI_ERR,
                SensorStatus1::FOG_X_ACQ_ERR,        SensorStatus1::FOG_Y_ACQ_ERR,
                SensorStatus1::FOG_Z_ACQ_ERR,        SensorStatus1::FOG_X_CRC_ERR,
                SensorStatus1::FOG_Y_CRC_ERR,        SensorStatus1::FOG_Z_CRC_ERR,
                SensorStatus1::TEMP_ACQ_ERR,         SensorStatus1::TEMP_THRESHOLD_ERR,
                SensorStatus1::DTEMP_THRESHOLD_ERR,  SensorStatus1::SENSOR_DATA_FIFO_ERR,
                SensorStatus1::SOURCE_POWER_ERR,     SensorStatus1::SOURCE_RECEPTION_ERR } },
            { "SensorStatus2",
              raw_data_.sensor_status.status_2,
              SensorStatus2::severity_message_pairs,
              { SensorStatus2::FOG_X_ERR, SensorStatus2::FOG_Y_ERR, SensorStatus2::FOG_Z_ERR, SensorStatus2::SOURCE_ERR,
                SensorStatus2::ACC_X_ERR, SensorStatus2::ACC_Y_ERR, SensorStatus2::ACC_Z_ERR, SensorStatus2::TEMP_ERR,
                SensorStatus2::DSP_OVERLOAD, SensorStatus2::ERR_INIT_CAN_ACC_X, SensorStatus2::ERR_INIT_CAN_ACC_Y,
                SensorStatus2::ERR_INIT_CAN_ACC_Z, SensorStatus2::MODELISATION_ERROR, SensorStatus2::DEGRADED_MODE,
                SensorStatus2::FAILURE_MODE } },
            { "UserStatus",
              raw_data_.ins_user_status.status,
              UserStatus::severity_message_pairs,
              { UserStatus::FOG_ANOMALY, UserStatus::ACC_ANOMALY, UserStatus::TEMPERATURE_ERR, UserStatus::CPU_OVERLOAD,
                UserStatus::DYNAMIC_EXCEDEED, UserStatus::SPEED_SATURATION, UserStatus::ALTITUDE_SATURATION,
                UserStatus::HRP_INVALID, UserStatus::DEGRADED_MODE, UserStatus::FAILURE_MODE } } }
        };

    // Check infos, warnings and errors
    bool navigation_ok = true;
    for (const auto& elem : status_tuples)
    {
      // Fill sets
      for (std::size_t i = 0; i < STATUS_BIT_AMOUNT; ++i)
      {
        if (std::get<1>(elem) & (1 << i))
        {
          if (std::get<2>(elem)[i].second == "")
            continue;
          if (std::get<2>(elem)[i].first == SeverityLevels::Info)
          {
            infos_.insert(std::get<2>(elem)[i].second);
          }
          else if (std::get<2>(elem)[i].first == SeverityLevels::Warn)
          {
            warnings_.insert(std::get<2>(elem)[i].second);
          }
          else if (std::get<2>(elem)[i].first == SeverityLevels::Error)
          {
            errors_.insert(std::get<2>(elem)[i].second);
          }
        }
      }

      // Check error bits to invalidate navigation
      for (const auto& bit : std::get<3>(elem))
        navigation_ok &= !(std::get<1>(elem) & (1 << bit));
    }

    // If PHINS initialized publish raw data, nav_sts, odom and tfs
    std::vector<double> pose{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    double north, east, depth;
    if (init_phins_)
    {
      if (navigation_ok)
      {
        ned_.geodetic2Ned(raw_data_.position.latitude, raw_data_.position.longitude, 0.0, north, east, depth);
        pose[0] = north;
        pose[1] = east;
        pose[2] = -raw_data_.position.altitude;
        pose[3] = raw_data_.attitude_quaternion.q0;
        pose[4] = raw_data_.attitude_quaternion.q1;
        pose[5] = raw_data_.attitude_quaternion.q2;
        pose[6] = raw_data_.attitude_quaternion.q3;
        return pose;
        // publishNavigation(stamp);
      }
      else
      {
		  std::cout << "NAVIGATION NOT OK" << std::endl;
		  // std::cout << "raw_data = " << raw_data_ << std::endl;
		  // std::cout << std::endl;
		  return std::vector<double>{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
	  }
    }
    else
    {
		std::cout << "PHINS NOT INITIALIZED" << std::endl;
	}
  }
  catch (const std::exception& ex)
  {
    ROS_WARN_STREAM("Error while parsing navigation output data: " << ex.what());
    return std::vector<double>{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    
  }
}

// // *****************************************************************************
// // Publishers
// // *****************************************************************************
// void IxbluePhins::publishNavigation(const ros::Time& stamp)
// {
//   // Compute NED
//   double north, east, depth;
//   ned_.geodetic2Ned(raw_data_.position.latitude, raw_data_.position.longitude, 0.0, north, east, depth);

//   // Fill up odometry msg
//   nav_msgs::Odometry odom;
//   odom.header.frame_id = frame_world_;
//   odom.header.stamp = stamp;

//   odom.pose.pose.position.x = north;
//   odom.pose.pose.position.y = east;
//   odom.pose.pose.position.z = -raw_data_.position.altitude;
//   odom.pose.pose.orientation.w = raw_data_.attitude_quaternion.q0;
//   odom.pose.pose.orientation.x = raw_data_.attitude_quaternion.q1;
//   odom.pose.pose.orientation.y = raw_data_.attitude_quaternion.q2;
//   odom.pose.pose.orientation.z = raw_data_.attitude_quaternion.q3;
//   odom.twist.twist.linear.x = raw_data_.velocity_v.vel_xv1;
//   odom.twist.twist.linear.y = raw_data_.velocity_v.vel_xv2;  // TODO: check sign
//   odom.twist.twist.linear.z = raw_data_.velocity_v.vel_xv3;
//   odom.twist.twist.angular.x =
//       cola2::utils::wrapAngle(cola2::utils::degreesToRadians(raw_data_.heading_roll_pitch_rate.roll_rate));
//   odom.twist.twist.angular.y =
//       cola2::utils::wrapAngle(cola2::utils::degreesToRadians(raw_data_.heading_roll_pitch_rate.pitch_rate));
//   odom.twist.twist.angular.z =
//       cola2::utils::wrapAngle(cola2::utils::degreesToRadians(raw_data_.heading_roll_pitch_rate.heading_rate));

//   Eigen::MatrixXd pose_cov = Eigen::MatrixXd::Zero(6, 6);
//   pose_cov(0, 0) = std::pow(raw_data_.position_std_dev.north_std_dev, 2);
//   pose_cov(1, 1) = std::pow(raw_data_.position_std_dev.east_std_dev, 2);
//   pose_cov(2, 2) = std::pow(raw_data_.position_std_dev.altitude_std_dev, 2);
//   pose_cov(0, 1) = raw_data_.position_std_dev.north_east_correlation;
//   pose_cov(1, 0) = raw_data_.position_std_dev.north_east_correlation;

//   pose_cov(3, 3) = std::pow(raw_data_.attitude_heading_std_dev.roll_std_dev, 2);
//   pose_cov(4, 4) = std::pow(raw_data_.attitude_heading_std_dev.pitch_std_dev, 2);
//   pose_cov(5, 5) = std::pow(raw_data_.attitude_heading_std_dev.heading_std_dev, 2);

//   Eigen::MatrixXd twist_cov = Eigen::MatrixXd::Zero(6, 6);
//   twist_cov(0, 0) = std::pow(raw_data_.velocity_g_std_dev.vel_north_std_dev, 2);  // TODO: Convert to vehicle frame?
//   twist_cov(1, 1) = std::pow(raw_data_.velocity_g_std_dev.vel_east_std_dev, 2);   // TODO: Convert to vehicle frame?
//   twist_cov(2, 2) = std::pow(raw_data_.velocity_g_std_dev.vel_up_std_dev, 2);     // TODO: Convert to vehicle frame?

//   twist_cov(3, 3) = std::pow(raw_data_.rotation_rate_std_dev.xv1, 2);
//   twist_cov(4, 4) = std::pow(raw_data_.rotation_rate_std_dev.xv2, 2);
//   twist_cov(5, 5) = std::pow(raw_data_.rotation_rate_std_dev.xv3, 2);

//   for (std::size_t i = 0; i < 6; ++i)
//   {
//     for (std::size_t j = 0; j < 6; ++j)
//     {
//       odom.pose.covariance.at(i * 6 + j) = pose_cov(i, j);
//       odom.twist.covariance.at(i * 6 + j) = twist_cov(i, j);
//     }
//   }

//   // Publish messages
//   pub_odom_.publish(odom);

//   // Publish _vehicle_ TF
//   tf::Transform transform;
//   transform.setOrigin(tf::Vector3(odom.pose.pose.position.x, odom.pose.pose.position.y, odom.pose.pose.position.z));
//   transform.setRotation(tf::Quaternion(odom.pose.pose.orientation.x, odom.pose.pose.orientation.y,
//                                        odom.pose.pose.orientation.z, odom.pose.pose.orientation.w));
//   tf_broadcast_.sendTransform(tf::StampedTransform(transform, stamp, frame_world_, frame_vehicle_));
// }

// *****************************************************************************
// Helper functions
// *****************************************************************************
void IxbluePhins::getConnectionConfig()
{
  bool ok = true;
  ok &= cola2::ros::getParam("~ip", connection_config_.ip, std::string("192.168.1.82"));
  ok &= cola2::ros::getParam("~nav_output_port", connection_config_.nav_output_port);
  ok &= cola2::ros::getParam("~socket_timeout", connection_config_.socket_timeout);
  if (!ok)
  {
    ROS_FATAL("Shutdown due to invalid connection parameters!");
    ros::shutdown();
  }
}

bool IxbluePhins::getConfig()
{
  // Load navigation params from ROS param server
  NavConfig temp_config;
  bool ok = true;
  ok &= cola2::ros::getParam(ns_ + "/navigator/ned_latitude", temp_config.ned_latitude_);
  ok &= cola2::ros::getParam(ns_ + "/navigator/ned_longitude", temp_config.ned_longitude_);

  // Check if valid
  if (!ok)
  {
    if (first_config_)
    {
      ROS_FATAL("Shutdown due to invalid navigation parameters!");
      ros::shutdown();
    }
    ROS_ERROR("Invalid navigation parameters! No changes applied");
    return false;
  }

  // Apply changes and display message
  nav_config_ = temp_config;
  ned_ = cola2::utils::NED(nav_config_.ned_latitude_, nav_config_.ned_longitude_, 0.0);
  first_config_ = false;
  ROS_INFO("Parameters loaded correctly from param server");
  return true;
}

// int main(int argc, char** argv)
// {
//   ros::init(argc, argv, "ixblue_phinsc3_ins");
//   IxbluePhins phins;
//   while (!ros::isShuttingDown())
//   {
//     phins.mainCallback();
//     ros::spinOnce();
//   }
//   return 0;
// }
