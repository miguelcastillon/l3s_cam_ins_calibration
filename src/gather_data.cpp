#include <math.h>
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/transform_listener.h>

#include <chrono>
#include <experimental/filesystem>
#include <fstream>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include <thread>

#include "l3s/computer_vision/calibration/calibration_pattern_points_extractor.h"
#include "l3s/computer_vision/calibration/chessboard_points_extractor.h"
#include "l3s/computer_vision/calibration/pinhole_distortion_camera_calibration.h"
#include "l3s/computer_vision/model/cameramodel.h"
#include "l3s/computer_vision/model/sensormodel.h"
#include "l3s/drivers/camera/pfcamera.h"
#include "l3s/drivers/ebhi/ebhi_arduino.h"
#include "l3s/geometry/point2.h"
#include "l3s/geometry/point3.h"
#include "readINS.h"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "cam_ins_calibration");
  ros::NodeHandle nh_("~");
  // ros::spinOnce();

  int exposure_time = 5000;
  std::string camera_ip = "192.168.100.100";
  std::string base_path = "/home/user/catkin_ws/src/l3s_cam_ins_calibration/";
  std::string sensor_model_file = base_path + "config/sensor_model.xml";
  std::string image_path = base_path + "images/";
  int time_out = 1000;

  cv::Size pattern_size = cv::Size(10, 10);
  cv::Size2d cell_size = cv::Size2d(0.056, 0.056);
  l3s::model::SensorModel<float>::Ptr sensor_model_ = std::make_shared<l3s::model::SensorModel<float>>();
  sensor_model_->load(sensor_model_file);
  l3s::model::CameraModel<float>::Ptr camera_model = sensor_model_->getCamera();

  // Set up camera
  l3s::drivers::PFCamera::Ptr pfcamera_ = std::make_shared<l3s::drivers::PFCamera>(camera_ip);
  pfcamera_->setAcquisitionMode(l3s::drivers::PFCamera::ACQUISITION_MODES::Continuous);
  pfcamera_->setTriggerSource(l3s::drivers::PFCamera::Line1);
  pfcamera_->setExposureMode(l3s::drivers::PFCamera::Timed);
  pfcamera_->setTriggerMode(l3s::drivers::PFCamera::TRIGGER_MODE::ON);
  // pfcamera_->setAcquisitionFrameRateEnable(true);
  // pfcamera_->setAcquisitionFrameRate(5);
  pfcamera_->set2DMode();
  pfcamera_->setExposureTime(exposure_time);
  pfcamera_->streamEnable();
  pfcamera_->startAcquisition();

  l3s::drivers::EBHI::Ptr ebhi_;
  ebhi_ = std::make_shared<l3s::drivers::EBHIArduino>(
      "/dev/ttyUSB1", l3s::drivers::SerialPort::BAUD_115200);  // TODO: make USB port a parameter

  cv::namedWindow("display window", CV_WINDOW_NORMAL | CV_WINDOW_KEEPRATIO);
  cv::resizeWindow("display window", 800, 425);

  IxbluePhins ins_;

  int saved_images = 0;
  int nb_images_needed = 150;
  l3s::calibration::CalibrationPatternPointsExtractor::Ptr points_extractor;
  points_extractor.reset(new l3s::calibration::ChessboardPointsExtractor(pattern_size, cell_size));
  int j = 0;
  std::vector<l3s::geometry::Transform<float>> poses_INS_vector(nb_images_needed);

  int count = 0;
  while (saved_images < nb_images_needed and ros::ok())
  {
    //if (count < 10)  // TODO: adjust this number to give enough time to camera (should be ~1Hz)
    //{
    //  std::vector<double> pose = ins_.mainCallback();  // TODO: Is this fast enough so that INS doesn't
    //                                                   // accumulate buffer?
    //                                                   // If not, reduce output frequency
    //  count++;
    //  continue;
    //}
    //else
    //{
      count = 0;
      cv::Mat pattern_image;
      try
      {
        ebhi_->triggerCamera();
        pfcamera_->grabImage(time_out).copyTo(pattern_image);

        // Read INS as soon as possible after taking image
        std::vector<double> pose = ins_.mainCallback();
        if (pose[0] == 0.0 and pose[1] == 0.0 and pose[2] == 0.0)
        {
			continue;
		}

        // std::cout << pose << std::endl;

        std::vector<l3s::geometry::Point2<float>> ip;
        std::vector<l3s::geometry::Point3<float>> op;
        points_extractor->getPoints(pattern_image, ip, op);
        cv::Mat image_show;
        pattern_image.copyTo(image_show);
        image_show = points_extractor->drawDetections();
        cv::imshow("display window", image_show);
        cv::waitKey(10);
        if (ip.size() == pattern_size.height * pattern_size.width)
        {
          // std::cout << j <<std::endl;
          if (j == 0)
          {
            j = 0;
            cv::imwrite(image_path + std::to_string(saved_images) + ".jpg", pattern_image);
            std::cout << "New image written! Remaining: " << nb_images_needed - saved_images - 1 << std::endl;
            Eigen::Matrix<float, 3, 1> translation;
            translation(0, 0) = pose[0];
            translation(1, 0) = pose[1];
            translation(2, 0) = pose[2];
            Eigen::Quaternion<float> q(pose[3], pose[4], pose[5], pose[6]);
            Eigen::Matrix<float, 3, 1> rpy = l3s::geometry::getRPY(q);
            l3s::geometry::Transform<float> pose_INS(translation, rpy);
            poses_INS_vector[saved_images] = pose_INS;
            saved_images++;
            if (saved_images == nb_images_needed)
            {
              std::ofstream file_ins_poses;
              file_ins_poses.open(base_path + "file_ins_poses.csv");
              for (int i = 0; i < nb_images_needed; i++)
              {
                file_ins_poses << std::setprecision(20) << poses_INS_vector[i] << "\n";
              }
              file_ins_poses.close();
            }
          }
          else
          {
            j++;
          }
        }
        points_extractor->clearDetection();
      }
      catch (l3s::Exception& ex)
      {
        L3S_LOG_WARN(ex.what());
        continue;
      }
      usleep(1000000);
      
    //}
    ros::spinOnce();
  }
  std::cout << "IMAGES SUCCESSFULLY GATHERED!" << std::endl << std::endl;
  cv::destroyWindow("display window");
  pfcamera_->stopAcquisition();

  return 0;
}
