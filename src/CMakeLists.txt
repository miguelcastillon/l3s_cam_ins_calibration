# Sensor Ui
#QT5_WRAP_CPP(l3suiros_moc l3suiros.h)
#add_library(l3suiros SHARED l3suiros.cpp ${l3suiros_moc} l3suiros.h)
#target_link_libraries(l3suiros ${catkin_LIBRARIES} libl3s_ql3sui)
#add_executable(sensor_ui sensor_ui.cpp)
#target_link_libraries(sensor_ui ${catkin_LIBRARIES} l3suiros)

# Sensor online triangulation
add_executable(gather_data gather_data.cpp readINS.cpp)
add_dependencies(gather_data ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(gather_data ${catkin_LIBRARIES} ${L3S_LIBRARIES} ${COLA2_LIB_LIBRARIES})

# Camera
#add_executable(camera camera.cpp)
#target_link_libraries(camera ${catkin_LIBRARIES} ${L3S_LIBRARIES})
