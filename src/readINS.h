/*
 * Copyright (c) 2020 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#include <cola2_lib/io/tcp_socket.h>
#include <cola2_lib/utils/angles.h>
#include <cola2_lib/utils/ned.h>
#include <cola2_lib_ros/param_loader.h>
#include <cola2_lib_ros/this_node.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <algorithm>
#include <array>
#include <cmath>
#include <cstdint>
#include <exception>
#include <iostream>
#include <memory>
#include <set>
#include <string>
#include <utility>
#include <vector>

#include "../include/ixblue_stdbin_bit_names.h"
#include "../include/ixblue_stdbin_ext_sensor_input.hpp"
#include "../include/ixblue_stdbin_navigation_telegram.hpp"
#include "../include/ixblue_stdbin_parser.hpp"

/**********************************************************************************************************************
 * ROS wrapper of PHINS driver to be integrated as a navigator node inside COLA2
 *
 * This node:
 * - Gets from configuration yaml all PHINS connection information (IP, input/output TCP ports) according to those
 *   ports configured through the web interface.
 * - Reads navigation output from PHINS and publish topics with PhinsRawData, Odometry and NavSts messages.
 * - Receives data from different external sensors and sends them to PHINS.
 * - Handles the diagnostics of received sensor data, including those of PHINS as well as all external sensors.
 *
 * NOTES:
 * - Navigation data in global coordinates from PHINS is transformed to a local NED system, initialized by setting a
 *   fixed lat,lon from config yaml.
 *   If no GPS signal is available and the PHINS has not been yet initialized, the NED origin will be set in the
 *   coordinates defined in the yaml file and the PHINS will be initialized by forcing a position fix in these
 *   coordinates. From then on, GPS data won't be taken into account (use_gps_data flag will be set to false) to avoid
 *   jumps in the navigation if eventually a GPS signal arrives.
 *
 * - This node assumes the DVL is connected directly to the INS and therefore does not pass data from the DVL to the
 *   PHINS. The subscription to the DVL topic is only for setting the altitude (which cannot be get from the PHINS raw
 *   data) and for diagnostic purposes.
 *
 **********************************************************************************************************************/

class IxbluePhins
{
private:
  // Node handle
  ros::NodeHandle nh_;

  // Publishers
  ros::Publisher pub_odom_;

  // Namespace and frames
  const std::string ns_, frame_world_, frame_vehicle_;

  // Config struct for PHINS connections
  struct
  {
    std::string ip;
    int nav_output_port;
    int socket_timeout;
  } connection_config_;

  // Struct with navigation parameters to be loaded from config file
  struct NavConfig
  {
    double ned_latitude_;
    double ned_longitude_;
  };
  NavConfig nav_config_;
  bool first_config_;

  // To store the parsed PHINS raw data
  IxblueStdBinRawData raw_data_;

  // Sockets
  std::shared_ptr<cola2::io::TcpSocket> socket_nav_out_;

  // TF broadcaster to publish world->vehicle transform
  tf::TransformBroadcaster tf_broadcast_;

  // Init flags
  bool init_phins_;

  // Times
  double last_phins_data_;
  double last_init_phins_time_;

  // NED
  cola2::utils::NED ned_;

  // Info, warnings and errors
  std::set<std::string> infos_, warnings_, errors_;

  /**
   * \brief Reads the configuration of the device and the navigator from the parameter server
   */
  bool getConfig();

  /**
   * \brief Reads the configuration of the device and the navigator from the parameter server
   */
  void getConnectionConfig();

  /**
   * \brief Establishes connection with the output navigation port of the Phins as well as the different input ports
   *        to send data from external sensors to the Phins.
   * \return True if minimal connections with navigation output, GPS and pressure sensor are
   *         correctly established. USBL and CTD connections are optional and therefore valid_connection will
   *         still be true regardless if connection to these sensors has been established
   */
  bool configurePhinsIOConnections();

  /**
   * \brief Sets the local NED origin if must be set from yaml file
   */
  void initFlagsAndDiagnostics();

  // ***************************************************************************
  // Publishers
  // ***************************************************************************

  /**
   * Publishes navigation information in Odometry
   */
  //   void publishNavigation(const ros::Time&);

public:
  /**
   * \brief Default constructor
   */
  IxbluePhins();

  /**
   * \brief Gets PHINS data output and stores it in raw_data_ handler
   */
  std::vector<double> mainCallback();
};
